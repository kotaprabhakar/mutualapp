**Synopsis**

An Android app to help connect you to your best of friends. Unlock the power of your network nexus. Share, discuss and get benefitted. Get your problem solved.
Wouldn't it be nice if you had a task or a deadline to meet, or just a query, and your network along with its network was at your disposal? 
Mutualing does exactly that!

**Motivation**

If you were to shift to a new city and were in search of a house for lease, wouldn't it be great if someone who is your good friend or a good friend of your friend
would help you with the best choices available in the city? Mutualing was born from this idea. We are here to help you create your own mini-nexus to benefit 
yourselves in the best possible way.

**Code Description**

This is the code for the Mutualing Android application. 
An Android MVC architecture connected to an XAMPP server running on a Linux AWS server.
The MVC architecture is constructed using a content provider, a sync adapter and broadcast receivers.
The content provider provides an API for the rest of the application to retrieve data from an internal database.
The sync adapter syncs the data on the web server with the internal database.
Broadcast receivers are invoked whenever the internal database is altered and the UI screen is updated.