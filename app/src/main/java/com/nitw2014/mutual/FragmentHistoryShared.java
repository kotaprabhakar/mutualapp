package com.nitw2014.mutual;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prabhakar on 21/08/16.
 */
public class FragmentHistoryShared extends Fragment {

    private FragmentActivity sharedFragmentContext;
    private List<FeedHistoryRecycler> feedHistoryRecyclerList;
    private RecyclerView mRecyclerView;
    private AdapterHistoryRecycler adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    Handler handler, uiHandler;
    HandlerThread thread;

    SharedObserver sharedObserver;

    @Override
    public void onAttach(Context context) {
        sharedFragmentContext = (FragmentActivity) context;
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        sharedFragmentContext.getContentResolver().unregisterContentObserver(sharedObserver);
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_shared,container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.shared_fragment_activity);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(sharedFragmentContext));
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout_shared);

        feedHistoryRecyclerList = new ArrayList<>();
        adapter = new AdapterHistoryRecycler(sharedFragmentContext, feedHistoryRecyclerList);
        mRecyclerView.setAdapter(adapter);

        thread = new HandlerThread("SharedBackgroundThread");
        thread.start();
        handler = new Handler(thread.getLooper());
        uiHandler = new Handler(sharedFragmentContext.getMainLooper());

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                ((ActivityHistory)sharedFragmentContext).syncDatabaseWithServer();
                updateData();
            }
        });

        sharedObserver = new SharedObserver(handler, sharedFragmentContext);
        sharedFragmentContext.getContentResolver().registerContentObserver(ProviderContractTask.SharedHistory.CONTENT_URI, true, sharedObserver);

        updateData();

        return view;
    }

    void updateData()
    {
        handler.post(new Runnable() {
            @Override
            public void run() {
                ((ActivityHistory)sharedFragmentContext).getDataFromContentProviderHistory(feedHistoryRecyclerList, ProviderContractTask.SharedHistory.CONTENT_URI, new String[]{ProviderContractTask.SharedHistory.TASK, ProviderContractTask.SharedHistory.SUBMITTER}, ProviderContractTask.SharedHistory.DEFAULT_SORT_ORDER);
                uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        });
    }

    //------------------------------------------INNER CLASSES---------------------------------------

    private class SharedObserver extends ContentObserver {
        Activity activity;
        Handler handler;

        SharedObserver(Handler handler, Activity activity){
            super(handler);
            this.activity = activity;
            this.handler = handler;
        }

        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {

            updateData();

        }
    }

}
