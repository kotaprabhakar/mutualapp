package com.nitw2014.mutual;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ExecutionException;

/**
 * Created by ssarawag on 12/7/2017.
 */


public class ActivityDeactivateAccount extends AppCompatActivity {
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    String emailID;
    int userID;
    EditText code;
    private DeactivateTask mAuthTask = null;
    String code_string;
    ProgressDialog progressDialog;
    private View mProgressView;
    private View mLoginFormView;
    BroadcastReceiver Successfullychange = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String Verify = intent.getStringExtra("Successfullychange");
            if(Verify.equals("true")){
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Account Deactivated!!", Toast.LENGTH_LONG).show();
                Intent loginActivityIntent = new Intent(getApplicationContext(), ActivityLogin.class);
                loginActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(loginActivityIntent);
                finish();
            }
            else{
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please connect to a network and try again!", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(Successfullychange);
        super.onDestroy();
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar mutualToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mutualToolbar);
        LocalBroadcastManager.getInstance(this).registerReceiver(Successfullychange, new IntentFilter("ChangePassword"));

        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        setContentView(R.layout.activity_deactivate_account);
        Button button = (Button) findViewById(R.id.SubmitVerify);
        code = (EditText)findViewById(R.id.code);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
        userID = sharedPreferences.getInt("UserID",0);
        emailID = sharedPreferences.getString("EmailID","String");
        assert button != null;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                code_string = code.getText().toString();
                Intent queryServiceIntent = new Intent(getApplicationContext(), ServiceQueryIntent.class);
                queryServiceIntent.setAction("com.nitw2014.action.VERIFY_DEACTIVATE_ACCOUNT");
                queryServiceIntent.putExtra("code", code_string);
                queryServiceIntent.putExtra("UserID",userID);
                queryServiceIntent.putExtra("EmailID", emailID);
                startService(queryServiceIntent);
                progressDialog = new ProgressDialog(com.nitw2014.mutual.ActivityDeactivateAccount.this);
                progressDialog.setMessage("Trying to Verify!");
                progressDialog.show();
            }
        });
        showProgress(true);
        mAuthTask = new DeactivateTask(emailID,userID);
        try {
            mAuthTask.execute((Void) null).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
    public class DeactivateTask extends AsyncTask<Void, Void, Boolean> {

        private final String mEmail;
        private final int muserid;
        boolean wrongPassword;
        int resultObject;

        DeactivateTask(String email,int userid) {
            mEmail = email;
            muserid = userid;
        }

        @Override
        protected Boolean doInBackground(Void... params) {

            try {

                String temporaryString, inputString = "";
                JSONObject jsonObject = new JSONObject().put("EmailID",mEmail).put("UserID",muserid);
                URL url = new URL(ServiceQueryIntent.URL_DEACTIVATE_ACCOUNT);
                HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
                Log.d("ActivityDeactivate", jsonObject.toString());
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
                outputStreamWriter.write(jsonObject.toString());
                outputStreamWriter.flush();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                Log.d("ActivityDeactivate","Reading userid");
                while((temporaryString = bufferedReader.readLine())!=null)
                {
                    inputString = inputString + temporaryString;
                }
                resultObject = new JSONObject(inputString).getInt("var");
                Log.d("ActivityLogin","UserID or Status: "+resultObject);
                if(resultObject == 2)
                    return false;
                else if(resultObject == 0){
                    wrongPassword = true;
                    return false;
                }
                else {
                    return true;
                }
            } catch (Exception e) {
                Log.d("ActivityLogin","Error Logging in");
                return false;
            }
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            showProgress(false);

            if (success) {
                SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("UserID",resultObject);
                editor.apply();
                Log.d("ActivityLogin","Applied SharedPreferences; ResultObject: "+ resultObject);
                finish();
            }
        }

        @Override
        protected void onCancelled() {
            showProgress(false);
        }
    }
}

