package com.nitw2014.mutual;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Process;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prabhakar on 21/08/16.
 */
public class FragmentHistoryAsked extends Fragment {

    private Activity askedFragmentContext;
    private RecyclerView mRecyclerView;
    private AdapterHistoryRecycler adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private List<FeedHistoryRecycler> feedHistoryRecyclerList;
    Handler handler, uiHandler;
    HandlerThread thread;

    AskedObserver askedObserver;

    @Override
    public void onAttach(Context context) {
        askedFragmentContext = (Activity)context;
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        askedFragmentContext.getContentResolver().unregisterContentObserver(askedObserver);
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_asked,container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.asked_recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(askedFragmentContext));
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout_asked);

        feedHistoryRecyclerList = new ArrayList<>();
        adapter = new AdapterHistoryRecycler(askedFragmentContext, feedHistoryRecyclerList);
        mRecyclerView.setAdapter(adapter);

        thread = new HandlerThread("AskedBackgroundThread");
        thread.start();
        handler = new Handler(thread.getLooper());
        uiHandler = new Handler(askedFragmentContext.getMainLooper());

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                ((ActivityHistory)askedFragmentContext).syncDatabaseWithServer();
                updateData();
            }
        });

        askedObserver = new AskedObserver(handler, askedFragmentContext);
        askedFragmentContext.getContentResolver().registerContentObserver(ProviderContractTask.AskedHistory.CONTENT_URI, true, askedObserver);

        updateData();

        return view;
    }

    void updateData()
    {
        handler.post(new Runnable() {
            @Override
            public void run() {
                ((ActivityHistory)askedFragmentContext).getDataFromContentProviderHistory(feedHistoryRecyclerList, ProviderContractTask.AskedHistory.CONTENT_URI, new String[]{ProviderContractTask.AskedHistory.SUBMITTER, ProviderContractTask.AskedHistory.TASK}, ProviderContractTask.AskedHistory.DEFAULT_SORT_ORDER);
                uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        });
    }

    //---------------------------------------INNER CLASSES------------------------------------------

     private class AskedObserver extends ContentObserver {
        Activity activity;
        Handler handler;

        AskedObserver(Handler handler, Activity activity){
            super(handler);
            this.activity = activity;
            this.handler = handler;
        }

        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {

            updateData();

        }
    }

}
