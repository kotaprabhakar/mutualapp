package com.nitw2014.mutual;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class Launcher extends AppCompatActivity {

    public static final String AUTHORITY = "com.nitw2014.mutual.taskprovider";
    public static final String ACCOUNT = "dummyaccount";
    public static final String ACCOUNT_TYPE = "mutualing.com";
    public static final long SECONDS_PER_MINUTE = 60L;
    public static final long SYNC_INTERVAL_IN_MINUTES = 60L;
    public static final long SYNC_INTERVAL = SECONDS_PER_MINUTE*SYNC_INTERVAL_IN_MINUTES;
    Account mAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        Toast.makeText(this, "I'm alive", Toast.LENGTH_LONG).show();
        Intent signupActivityIntent1 = new Intent(getApplicationContext(), ActivityLogin.class);
        Intent signupActivityIntent = new Intent(getApplicationContext(), ActivitySlideIntro.class);
        Intent mainActivityIntent = new Intent(getApplicationContext(), ActivityDashboard.class);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        int sharedPrefInt, finishIntro;

        SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);

        sharedPrefInt = sharedPreferences.getInt("UserID", -1);
        finishIntro = sharedPreferences.getInt("IntroDone", -1);

        Log.d("Launcher",""+sharedPrefInt);

        /*
        mAccount = createSyncAccount(this);
        if(mAccount != null) {
            Log.d("LauncherActivity","Starting Periodic Sync");
            ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);
            ContentResolver.addPeriodicSync(mAccount, AUTHORITY, Bundle.EMPTY, SYNC_INTERVAL);
        }
        */

        if(finishIntro == -1){

            startActivity(signupActivityIntent);

        }else {
            if (sharedPrefInt == -1) {
                startActivity(signupActivityIntent1);
            }
            else{
                mainActivityIntent.putExtra("UserID", sharedPrefInt);
                startActivity(mainActivityIntent);
            }
        }

        finish();
    }

    public static Account createSyncAccount(Context context)
    {
        Account newAccount = new Account(ACCOUNT, ACCOUNT_TYPE);
        AccountManager accountManager = (AccountManager)context.getSystemService(ACCOUNT_SERVICE);
        if(accountManager.addAccountExplicitly(newAccount, null, null))
        {
            return newAccount;
        }else
        {
            Log.d("LauncherActivity","Account: "+accountManager.getAccounts()[0]);
            return accountManager.getAccounts()[0];
        }
    }

}
