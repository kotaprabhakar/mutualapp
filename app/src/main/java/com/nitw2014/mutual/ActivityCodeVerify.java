package com.nitw2014.mutual;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by ssarawag on 12/7/2017.
 */


public class ActivityCodeVerify  extends AppCompatActivity {
        /**
         * ATTENTION: This was auto-generated to implement the App Indexing API.
         * See https://g.co/AppIndexing/AndroidStudio for more information.
         */

        EditText newpassword;
        String newpassword_string;
        EditText code;
        String code_string;
        ProgressDialog verifyProgressDialog;
        BroadcastReceiver Successfullychange = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String Verify = intent.getStringExtra("Successfullychange");
                if(Verify.equals("true")){
                    verifyProgressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Please proceed to login!", Toast.LENGTH_LONG).show();
                    Intent loginActivityIntent = new Intent(getApplicationContext(), ActivityLogin.class);
                    loginActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(loginActivityIntent);
                    finish();
                }
                else{
                    verifyProgressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Please connect to a network and try again!", Toast.LENGTH_LONG).show();
                }
            }
        };

        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            //return super.onCreateOptionsMenu(menu);
            getMenuInflater().inflate(R.menu.menu_history, menu);
            return true;
        }

        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            super.onActivityResult(requestCode,resultCode,data);
        }

        @Override
        protected void onDestroy() {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(Successfullychange);
            super.onDestroy();
        }

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            Toolbar mutualToolbar = (Toolbar)findViewById(R.id.toolbar);
            setSupportActionBar(mutualToolbar);
            LocalBroadcastManager.getInstance(this).registerReceiver(Successfullychange, new IntentFilter("ChangePassword"));

            getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            setContentView(R.layout.activity_verify_password);
            Button button = (Button) findViewById(R.id.SubmitVerify);
            code = (EditText)findViewById(R.id.code);
            newpassword = (EditText)findViewById(R.id.new_password);

            assert button != null;
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    newpassword_string = newpassword.getText().toString();
                    code_string = code.getText().toString();
                    Intent queryServiceIntent = new Intent(getApplicationContext(), ServiceQueryIntent.class);
                    queryServiceIntent.setAction("com.nitw2014.action.VERIFY_CHANGE_PASSWORD");
                    queryServiceIntent.putExtra("code", code_string);
                    queryServiceIntent.putExtra("password", newpassword_string);
                    startService(queryServiceIntent);
                    verifyProgressDialog = new ProgressDialog(com.nitw2014.mutual.ActivityCodeVerify.this);
                    verifyProgressDialog.setMessage("Trying to Verify!");
                    verifyProgressDialog.show();
                }
            });

        }
        @Override
        public void onStart() {
            super.onStart();
        }

        @Override
        public void onStop() {
            super.onStop();
        }
    }

