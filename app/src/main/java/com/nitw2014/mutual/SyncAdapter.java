package com.nitw2014.mutual;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SyncResult;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by prabhakar on 30/09/16.
 */

class SyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String URL_DASHBOARD_DATA = "http://www.mutualing.com/uservote/admin_edit/getAssignedTask";
    private static final String URL_GET_SHARED_TASK = "http://www.mutualing.com/uservote/admin_edit/getSharedTask";
    private static final String URL_GET_ASKED_TASK = "http://www.mutualing.com/uservote/admin_edit/getTask";
    private static final String URL_GET_FRIENDS = "http://www.mutualing.com/uservote/admin_edit/getFriendList";
    private static final String LATEST_DATE_ASKED = "LatestDateAsked";
    private static final String LATEST_DATE_SHARED = "LatestDateShared";
    private static final String LATEST_DATE_PERFORMED = "LatestDatePerformed";
    private static final String LATEST_DATE_FRIENDS = "LatestDateFriends";
    private static final String DEFAULT_STRING = "NaN";
    private static final String TIMESTAMP = "timestamp";

    private ContentResolver contentResolver;
    Context context;
    private String storedDateAsked, storedDateShared, storedDatePerformed, storedDateFriends;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private SharedPreferences sharedPreferences;

    SyncAdapter(Context context, boolean autoInitialize){
        super(context, autoInitialize);
        this.context = context;
        contentResolver = context.getContentResolver();
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs){
        super(context, autoInitialize, allowParallelSyncs);
        contentResolver = context.getContentResolver();
        this.context = context;
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

        sharedPreferences = context.getSharedPreferences(context.getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
        storedDateAsked = sharedPreferences.getString(LATEST_DATE_ASKED, DEFAULT_STRING);
        storedDateShared = sharedPreferences.getString(LATEST_DATE_SHARED, DEFAULT_STRING);
        storedDatePerformed = sharedPreferences.getString(LATEST_DATE_PERFORMED, DEFAULT_STRING);
        storedDateFriends = sharedPreferences.getString(LATEST_DATE_FRIENDS, DEFAULT_STRING);

        //Dashboard
        handleActionQueryDashboardData();

        //Shared Tasks
        handleActionQueryGetSharedTask();

        //Asked Tasks
        handleActionQueryGetAskedTasks();

        //Friends
        handleActionQueryGetFriends();

    }

    private JSONArray networkQuery(String URL) throws JSONException, IOException {
        String temporaryString, inputString = "";
        SharedPreferences sharedPreferences = context.getSharedPreferences(context.getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
        int userID = sharedPreferences.getInt("UserID", 0);
        JSONObject jsonObject = new JSONObject().put("UserID", userID);
        URL url = new URL(URL);
        HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
        urlConnection.setRequestMethod("POST");
        urlConnection.setDoInput(true);
        urlConnection.setDoOutput(true);
        urlConnection.connect();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
        outputStreamWriter.write(jsonObject.toString());
        outputStreamWriter.flush();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        while((temporaryString = bufferedReader.readLine())!=null)
        {
            inputString = inputString + temporaryString;
        }

        return ((new JSONObject(inputString)).getJSONArray("var"));
    }

    private Uri handleActionQueryGetFriends()
    {
        Uri newUri = null;

        try{

            String dataDate_String;
            Date dataDate, storedDate = (!storedDateFriends.equals(DEFAULT_STRING))?dateFormat.parse(storedDateFriends): null, tempDate = storedDate;

            JSONArray inputArray = networkQuery(URL_GET_FRIENDS);
            Log.d("SyncAdapter", "Friends: " + inputArray.toString());
            for(int i = 0; i < inputArray.length(); i++){
                JSONObject tempJSON = inputArray.getJSONObject(i);
                dataDate_String = tempJSON.getString(TIMESTAMP);
                dataDate = dateFormat.parse(dataDate_String);

                if (storedDateFriends.equals(DEFAULT_STRING) || dataDate.after(storedDate)) {
                    ContentValues newValues = new ContentValues();
                    newValues.put(ProviderContractTask.FriendsList.FRIEND, tempJSON.get("first_name").toString() + " " + tempJSON.get("last_name").toString());
                    newValues.put(ProviderContractTask.FriendsList.FRIEND_ID, tempJSON.get("frndId").toString());
                    newUri = contentResolver.insert(ProviderContractTask.FriendsList.CONTENT_URI, newValues);
                }

                if(tempDate == null || dataDate.after(tempDate)) {
                    tempDate = dataDate;
                    (sharedPreferences.edit()).putString(LATEST_DATE_FRIENDS, dataDate_String).apply();
                    Log.d("SyncAdapter", dataDate_String);
                }

            }
        }catch(Exception ex){
            Log.d("SyncAdapter","Friends-Got an exception");
            if(ex.getMessage() != null)
                Log.d("SyncAdapter",ex.getMessage());
        }

        return newUri;
    }

    /**
     */
    private Uri handleActionQueryGetAskedTasks(){
        Uri newUri = null;
        try{
            String dataDate_String;
            Date dataDate, storedDate = (!storedDateAsked.equals(DEFAULT_STRING))?dateFormat.parse(storedDateAsked): null, tempDate = storedDate;

            Log.d("SyncAdapter", "AskedTask Stored- " + storedDateAsked);

            JSONArray inputArray = networkQuery(URL_GET_ASKED_TASK);

            Log.d("SyncAdapter", "AskedTask: " + inputArray.toString());

            for(int i = 0; i < inputArray.length(); i++){
                JSONObject tempJSON = inputArray.getJSONObject(i);
                dataDate_String = tempJSON.getString(TIMESTAMP);
                dataDate = dateFormat.parse(dataDate_String);

                if (storedDateAsked.equals(DEFAULT_STRING) || dataDate.after(storedDate)) {

                    ContentValues newValues = new ContentValues();
                    newValues.put(ProviderContractTask.AskedHistory.TASK, tempJSON.get("task_description").toString());
                    newValues.put(ProviderContractTask.AskedHistory.TASK_ID, tempJSON.get("id").toString());
                    newValues.putNull(ProviderContractTask.AskedHistory.SUBMITTER);
                    newValues.putNull(ProviderContractTask.AskedHistory.SUBMITTER_ID);
                    newUri = contentResolver.insert(ProviderContractTask.AskedHistory.CONTENT_URI, newValues);

                }

                if(tempDate == null || dataDate.after(tempDate)) {
                    tempDate = dataDate;
                    (sharedPreferences.edit()).putString(LATEST_DATE_ASKED, dataDate_String).apply();
                    Log.d("SyncAdapter", dataDate_String);
                }

            }

        }catch(Exception ex){
            Log.d("SyncAdapter","AskedHistory-Got an exception");
            if(ex.getMessage() != null)
                Log.d("SyncAdapter",ex.getMessage());
        }
        return newUri;
    }

    private Uri handleActionQueryGetSharedTask(){
        Uri newUri = null;
        try{
            String dataDate_String;
            Date dataDate, storedDate = (!storedDateShared.equals(DEFAULT_STRING))?dateFormat.parse(storedDateShared):null, tempDate = storedDate;

            Log.d("SyncAdapter", "SharedTask Stored- " + storedDateShared);

            JSONArray inputArray = networkQuery(URL_GET_SHARED_TASK);
            Log.d("SyncAdapter", "SharedTask: " + inputArray.toString());

            for(int i = 0; i < inputArray.length(); i++){
                JSONObject tempJSON = inputArray.getJSONObject(i);
                dataDate_String = tempJSON.getString(TIMESTAMP);
                dataDate = dateFormat.parse(dataDate_String);

                if (storedDateShared.equals(DEFAULT_STRING) || dataDate.after(storedDate)) {

                    ContentValues newValues = new ContentValues();
                    newValues.put(ProviderContractTask.SharedHistory.TASK, tempJSON.get("task_description").toString());
                    newValues.put(ProviderContractTask.SharedHistory.TASK_ID, tempJSON.get("task_id").toString());
                    newValues.put(ProviderContractTask.SharedHistory.SUBMITTER, tempJSON.get("first_name").toString() + " " + tempJSON.get("last_name").toString());
                    newValues.putNull(ProviderContractTask.SharedHistory.SUBMITTER_ID);
                    newUri = contentResolver.insert(ProviderContractTask.SharedHistory.CONTENT_URI, newValues);

                }

                if(tempDate == null || dataDate.after(tempDate)) {
                    tempDate = dataDate;
                    (sharedPreferences.edit()).putString(LATEST_DATE_SHARED, dataDate_String).apply();
                    Log.d("SyncAdapter", dataDate_String);
                }

            }

        }catch(Exception ex){
            Log.d("SyncAdapter","SharedHistory-Got an exception");
            if(ex.getMessage() != null)
                Log.d("SyncAdapter",ex.getMessage());
        }
        return newUri;

    }

    private Uri handleActionQueryDashboardData(){
        Uri newUri = null;
        try{
            String dataDate_String;
            Date dataDate, storedDate = (!storedDatePerformed.equals(DEFAULT_STRING))?dateFormat.parse(storedDatePerformed):null, tempDate = storedDate;

            Log.d("SyncAdapter", "PerformedTask Stored- " + storedDatePerformed);

            JSONArray inputArray = networkQuery(URL_DASHBOARD_DATA);
            Log.d("SyncAdapter", "PerformedTask: " + inputArray.toString());

            for(int i = 0; i < inputArray.length(); i++){
                JSONObject tempJSON = inputArray.getJSONObject(i);
                dataDate_String = tempJSON.getString(TIMESTAMP);
                dataDate = dateFormat.parse(dataDate_String);

                if (storedDatePerformed.equals(DEFAULT_STRING) || dataDate.after(storedDate)) {

                    if (tempJSON.get("task_answer").toString().equals(null) || tempJSON.get("task_answer").toString().equals("")) {
                        ContentValues newValues = new ContentValues();
                        newValues.put(ProviderContractTask.TasksAssigned.TASK, tempJSON.get("task_description").toString());
                        newValues.putNull(ProviderContractTask.TasksAssigned.TASK_ID);
                        newValues.put(ProviderContractTask.TasksAssigned.SUBMITTER, tempJSON.get("first_name").toString() + " " + tempJSON.get("last_name").toString());
                        newValues.putNull(ProviderContractTask.TasksAssigned.SUBMITTER_ID);
                        newUri = contentResolver.insert(ProviderContractTask.TasksAssigned.CONTENT_URI, newValues);
                    } else {
                        ContentValues newValues = new ContentValues();
                        newValues.put(ProviderContractTask.PerformedHistory.TASK, tempJSON.get("task_description").toString());
                        newValues.putNull(ProviderContractTask.PerformedHistory.TASK_ID);
                        newValues.put(ProviderContractTask.PerformedHistory.ANSWER, tempJSON.get("task_answer").toString());
                        newValues.put(ProviderContractTask.PerformedHistory.SUBMITTER, tempJSON.get("first_name").toString() + " " + tempJSON.get("last_name").toString());
                        newValues.putNull(ProviderContractTask.PerformedHistory.SUBMITTER_ID);
                        newUri = contentResolver.insert(ProviderContractTask.PerformedHistory.CONTENT_URI, newValues);
                    }

                }

                if(tempDate == null || dataDate.after(tempDate)) {
                    tempDate = dataDate;
                    (sharedPreferences.edit()).putString(LATEST_DATE_PERFORMED, dataDate_String).apply();
                    Log.d("SyncAdapter", dataDate_String);
                }

            }

        }catch(Exception ex){
            Log.d("SyncAdapter","PerformedHistory-Got an exception");
            if(ex.getMessage() != null)
                Log.d("SyncAdapter",ex.getMessage());
        }
        return newUri;
    }

}
