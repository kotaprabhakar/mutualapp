package com.nitw2014.mutual;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ActivitySlideIntro extends AppCompatActivity {

    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_intro);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        mViewPager =  (ViewPager) findViewById(R.id.slide_intro_viewpager);
        mViewPager.setAdapter(new AdapterSlideIntro(getSupportFragmentManager()));

    }
}
