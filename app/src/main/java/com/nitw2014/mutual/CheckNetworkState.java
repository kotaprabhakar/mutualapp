package com.nitw2014.mutual;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Created by prabhakar on 12/10/16.
 */

public class CheckNetworkState {

    static ConnectivityManager connectivityManager;
    static NetworkInfo networkInfo;

    public static boolean checkNetworkState(Context context){
        connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        networkInfo = connectivityManager.getActiveNetworkInfo();
        if(networkInfo!=null && networkInfo.isConnected()){
            return true;
        }
        Toast.makeText(context, "Please connect to the Internet!", Toast.LENGTH_LONG).show();
        return false;
    }

}
