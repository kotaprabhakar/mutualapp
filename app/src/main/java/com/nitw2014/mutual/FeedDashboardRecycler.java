package com.nitw2014.mutual;

/**
 * Created by prabhakar on 28/08/16.
 */
public class FeedDashboardRecycler {
    String fromTextViewString;
    String taskTextViewString;

    public String getFromTextViewString() {
        return fromTextViewString;
    }

    public String getTaskTextViewString() {
        return taskTextViewString;
    }

    public void setFromTextViewString(String fromTextViewString) {
        this.fromTextViewString = fromTextViewString;
    }

    public void setTaskTextViewString(String taskTextViewString) {
        this.taskTextViewString = taskTextViewString;
    }
}
