package com.nitw2014.mutual;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Sai Karthik on 6/12/2016.
 */
public class FragmentSignupEducation extends Fragment {
    String tempText;
    FragmentActivity educationFragmentContext;
    EditText educationEditText;
    Button button5, button6;
    AdapterSignupEducation adapterSignupEducation;
    AdapterSignupDetails adapterSignupDetails;
    final ArrayList<String> qualifications = new ArrayList<>();

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        adapterSignupDetails = (AdapterSignupDetails) args.getSerializable("AdapterObject");
    }

    public class AdapterSignupEducation extends ArrayAdapter<String> {
        Context applicationContext;
        int layoutResourceId;
        ArrayList<String> placesList;
        AdapterSignupEducation adapterObject;
        AdapterSignupEducation(Context context, int layoutResourceId, ArrayList<String> placesList){
            super(context, layoutResourceId, placesList);
            applicationContext = context;
            this.layoutResourceId = layoutResourceId;
            this.placesList = placesList;
            adapterObject = this;
        }

        @NonNull
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = ((Activity) applicationContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }
            String textToDisplay = placesList.get(position);
            TextView ticketText = (TextView) convertView.findViewById(R.id.ticket_text);
            Button closeButton = (Button) convertView.findViewById(R.id.delete_ticket);
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    placesList.remove(position);
                    adapterObject.notifyDataSetChanged();
                }
            });
            ticketText.setText(textToDisplay);
            return convertView;
        }
    }

    @Override
    public void onAttach(Context context) {
        educationFragmentContext = (FragmentActivity)context;
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup_education,container,false);
        educationFragmentContext.setTitle("Enter the details of your Education");
        final ListView dynamicLinearLayout = (ListView) view.findViewById(R.id.dynamic_linear_edittexts);
        button5 = (Button) view.findViewById(R.id.button5);
        button6 = (Button)view.findViewById(R.id.button6);
        educationEditText = (EditText)view.findViewById(R.id.education_edit_text);
        adapterSignupEducation = new AdapterSignupEducation(educationFragmentContext, R.layout.ticket_layout, qualifications);
        dynamicLinearLayout.setAdapter(adapterSignupEducation);

        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tempText = educationEditText.getText().toString();
                if((!tempText.equals("")) && tempText != null) {
                    qualifications.add(tempText);
                }
                educationEditText.setText("");
                adapterSignupEducation.notifyDataSetChanged();
            }
        });
        button6.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(CheckNetworkState.checkNetworkState(educationFragmentContext))
                    executeOnClickButton6();

            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((ActivityDetails)getActivity()).setTitleView("Enter Universities you attended!");

    }

    void executeOnClickButton6(){
        Intent queryServiceIntent = new Intent(educationFragmentContext, ServiceQueryIntent.class);
        queryServiceIntent.setAction("com.nitw2014.mutual.action.SET_EDUCATION");
        queryServiceIntent.putExtra("Education",qualifications);
        educationFragmentContext.startService(queryServiceIntent);

        adapterSignupDetails.detailsViewPager.setCurrentItem(3);

        button6.setOnClickListener(null);

    }

}
