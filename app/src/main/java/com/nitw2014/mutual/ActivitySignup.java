package com.nitw2014.mutual;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class ActivitySignup extends AppCompatActivity {

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    EditText emailID;
    String emailID_string;
    EditText phNumber;
    String phNumber_string;
    EditText password;
    String password_string;
    EditText firstName;
    String firstName_string;
    EditText lastName;
    String lastName_string;
    ProgressDialog signupProgressDialog;
    BroadcastReceiver Signup1Success = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String Signup1 = intent.getStringExtra("SuccessfulSignup1");
            if(Signup1.equals("true")){
                signupProgressDialog.dismiss();
                Intent detailsActivityIntent = new Intent(getApplicationContext(), ActivityDetails.class);
                detailsActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(detailsActivityIntent);
                finish();
            }
            else{
                signupProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please connect to a network and try again!", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(Signup1Success);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar mutualToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mutualToolbar);
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();

        LocalBroadcastManager.getInstance(this).registerReceiver(Signup1Success, new IntentFilter("Signup1"));

        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        //FacebookSdk.sdkInitialize(getApplicationContext());
        //callbackManager = CallbackManager.Factory.create();
        //AppEventsLogger.activateApp(getApplication());
        setContentView(R.layout.activity_signup);
        Button button = (Button) findViewById(R.id.signupButton);
        emailID = (EditText)findViewById(R.id.emailID);
        phNumber = (EditText)findViewById(R.id.phNumber);
        password = (EditText)findViewById(R.id.passwordEdit);
        firstName = (EditText) findViewById(R.id.firstName);
        lastName = (EditText) findViewById(R.id.lastName);

        assert button != null;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailID_string = emailID.getText().toString();
                phNumber_string = phNumber.getText().toString();
                password_string = password.getText().toString();
                firstName_string = firstName.getText().toString();
                lastName_string = lastName.getText().toString();
                Intent queryServiceIntent = new Intent(getApplicationContext(), ServiceQueryIntent.class);
                queryServiceIntent.setAction("com.nitw2014.mutual.action.SET_DETAILS");
                queryServiceIntent.putExtra("EmailID", emailID_string);
                queryServiceIntent.putExtra("PhNumber", phNumber_string);
                queryServiceIntent.putExtra("Password", password_string);
                queryServiceIntent.putExtra("FirstName", firstName_string);
                queryServiceIntent.putExtra("LastName", lastName_string);
                editor.putString("FirstName", firstName_string);
                editor.putString("LastName", lastName_string);
                editor.apply();
                //REMOVE FOLLOWING CODE IMMEDIATELY ----START
                //Intent mainActivityIntent = new Intent(getApplicationContext(), ActivityDashboard.class);
                //REMOVE FOLLOWING CODE IMMEDIATELY ----END
                startService(queryServiceIntent);
                signupProgressDialog = new ProgressDialog(ActivitySignup.this);
                signupProgressDialog.setMessage("Trying to Signup!");
                signupProgressDialog.show();
                //startActivity(mainActivityIntent);
            }
        });

    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
