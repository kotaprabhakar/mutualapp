package com.nitw2014.mutual;

/**
 * Created by prabhakar on 27/08/16.
 */
class FeedHistoryRecycler {

    private String fromUserString;
    private String mainTaskString;

    String getFromUserString()
    {
        return fromUserString;
    }

    String getMainTaskString()
    {
        return mainTaskString;
    }

    void setFromUserString(String fromUserString){
        this.fromUserString = fromUserString;
    }

    void setMainTaskString(String mainTaskString){
        this.mainTaskString = mainTaskString;
    }

}
