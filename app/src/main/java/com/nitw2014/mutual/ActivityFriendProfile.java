package com.nitw2014.mutual;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Looper;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import de.hdodenhof.circleimageview.CircleImageView;

public class ActivityFriendProfile extends AppCompatActivity {

    public static final String URL_ADD_FRIEND = "http://www.mutualing.com/uservote/admin_edit/addFriend";

    Toolbar toolbar_friend_profile;
    String userName;
    String searchID;
    AppBarLayout appBarLayout;
    CircleImageView circleImageView;
    Switch addFriendSwitch;
    Context context;
    Handler mHandler;
    ProgressDialog progressDialog;
    EditText places;

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    public enum RequestStatus {
        FRIENDS,
        REQUESTED,
        ACCEPT
    }
    BroadcastReceiver Successfullysent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String Signup1 = intent.getStringExtra("Successfullysent");
            if(Signup1.equals("true")){
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please check your email for code!", Toast.LENGTH_LONG).show();
                Intent codeVerifyActivityIntent = new Intent(getApplicationContext(), ActivityCodeVerify.class);
                //codeVerifyActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(codeVerifyActivityIntent);
                //finish();
            }
            else{
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please connect to a network and try again!", Toast.LENGTH_LONG).show();
            }
        }
    };
    BroadcastReceiver GetDetails = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            //String Signup = intent.getStringExtra("GotDetails");
            try {
                String jsonArrayString = intent.getStringExtra("JSONArrayString");
                JSONArray jsonArray = new JSONArray(jsonArrayString);
                places.setText("hello");
            }
            catch (Exception e)
            {
                Log.d("ActivityFriendProfile","Exception in receiving search query: " + e.getMessage());
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        context = this;
        Intent intent = new Intent(this, ServiceQueryIntent.class);
        intent.setAction(ServiceQueryIntent.QUERY_GET_USER_COMPLETE_DETAILS);
        intent.putExtra("SearchID",getIntent().getStringExtra("SearchID"));
        startService(intent);
        userName = getIntent().getStringExtra("UserName");
        searchID = getIntent().getStringExtra("SearchID");
        Log.d("ActivityFriendProfile", userName + " " + searchID);
        places = (EditText) findViewById(R.id.place_been);
        circleImageView = (CircleImageView) findViewById(R.id.profile_pic);
        addFriendSwitch = (Switch) findViewById(R.id.add_friend_switch);
        mHandler = new Handler(Looper.getMainLooper());
        LocalBroadcastManager.getInstance(this).registerReceiver(Successfullysent, new IntentFilter("SentEmail"));
        LocalBroadcastManager.getInstance(this).registerReceiver(GetDetails, new IntentFilter("GotDetails"));
        addFriendSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    Toast.makeText(context, "Adding friend", Toast.LENGTH_LONG).show();
                    new addFriendAsync().execute();
                }else{

                }
            }
        });

        /*
        appBarLayout = (AppBarLayout) findViewById(R.id.friend_profile_appbar);
        appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                Log.d("STATE", state.name());
                if(state.name().equals("COLLAPSED"))
                    circleImageView.setVisibility(View.GONE);
                else
                    circleImageView.setVisibility(View.VISIBLE);
            }
        });
        */
        /*
        belowStatusBar = (RelativeLayout) findViewById(R.id.below_statusbar_friend_profile);
        AppBarLayout.LayoutParams lp = (AppBarLayout.LayoutParams) belowStatusBar.getLayoutParams();
        lp.height = getStatusBarHeight();
        */


        toolbar_friend_profile = (Toolbar) findViewById(R.id.toolbar_friend_profile);
        toolbar_friend_profile.setTitle(userName);
        toolbar_friend_profile.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar_friend_profile);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitleColor(getResources().getColor(R.color.white));
        getSupportActionBar().setHomeAsUpIndicator(getDrawable(R.drawable.ic_action_arrow_back));

    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }


    public class addFriendAsync extends AsyncTask<Void,Void,Void>{

        int result;
// change done.
        @Override
        protected Void doInBackground(Void... voids) {

            try{
                String temporaryString, inputString = "";
                SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
                int userID = sharedPreferences.getInt("UserID",0);
                JSONObject jsonObject = new JSONObject().put("UserID", userID).put("SearchID", searchID);
                URL url = new URL(URL_ADD_FRIEND);
                HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
                outputStreamWriter.write(jsonObject.toString());
                outputStreamWriter.flush();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while((temporaryString = bufferedReader.readLine())!=null)
                {
                    inputString = inputString + temporaryString;
                }
                result = new JSONObject(inputString).getInt("var");
            }catch(Exception ex){
                Log.d("ActivityFriendProfile", "Error: "+ex.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("ActivityFriendProfile", result + "");
            if(result == 1)
                Toast.makeText(context, "Added Friend", Toast.LENGTH_LONG).show();
            else
                Toast.makeText(context, "Could not add Friend", Toast.LENGTH_LONG).show();
            super.onPostExecute(aVoid);
        }
    }

    public abstract class AppBarStateChangeListener implements AppBarLayout.OnOffsetChangedListener {

        private State mCurrentState = State.IDLE;

        @Override
        public final void onOffsetChanged(AppBarLayout appBarLayout, int i) {
            if (i == 0) {
                if (mCurrentState != State.EXPANDED) {
                    onStateChanged(appBarLayout, State.EXPANDED);
                }
                mCurrentState = State.EXPANDED;
            } else if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
                if (mCurrentState != State.COLLAPSED) {
                    onStateChanged(appBarLayout, State.COLLAPSED);
                }
                mCurrentState = State.COLLAPSED;
            } else {
                if (mCurrentState != State.IDLE) {
                    onStateChanged(appBarLayout, State.IDLE);
                }
                mCurrentState = State.IDLE;
            }
        }

        public abstract void onStateChanged(AppBarLayout appBarLayout, State state);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case R.id.home_menu:
                Intent historyIntent = new Intent(this, ActivityDashboard.class);
                startActivity(historyIntent);
                return true;
            case R.id.home_deactivation:
                Intent deactivate = new Intent(this, ActivityDeactivateAccount.class);
                startActivity(deactivate);
                return true;

            case android.R.id.home:
                this.finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }
}
