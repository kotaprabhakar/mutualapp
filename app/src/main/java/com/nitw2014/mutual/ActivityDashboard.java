package com.nitw2014.mutual;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.ContentObserver;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.RectF;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MarginLayoutParamsCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.vision.text.Line;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.R.id.list;

public class ActivityDashboard extends RecyclerViewActivity {

    private static final int REQ_CODE_PICK_IMAGE = 100;
    private static final Uri URI_TO_OBSERVE = ProviderContractTask.TasksAssigned.CONTENT_URI;

    AdapterDashboardRecycler adapterDashboardRecycler = null;
    FeedDashboardRecycler feedDashboardRecycler = null;
    List<FeedDashboardRecycler> feedDashboardRecyclerList = null;
    SwipeRefreshLayout swipeRefreshLayoutDashboard;
    DashboardObserver dashboardObserver;
    FloatingActionButton fab_dashboard;
    SharedPreferences preferences;
    //private ListView filteringListView;
    private RelativeLayout mDrawerContentRight;
    ListView list;
    DrawerLayout mDrawerLayout;
    LinearLayout mDrawerList;
    Context activityContext;
    Cursor cursor;
    private SimpleAdapter leftNavAdapter;
    public static ArrayList<HashMap<String, Object>> filteringList = new ArrayList<HashMap<String, Object>>();

    String[] web = {
            "Profile",
            "Ask",
            "Mutual center",
            "History",
            "Help",
            "Log out"
    } ;
    Integer[] imageId = {
            R.drawable.profile_copy,
            R.drawable.background_design,
            R.drawable.mutualing_center,
            R.drawable.histroy,
            R.drawable.arrow,
            R.drawable.logout_copy};


    public static final String AUTHORITY = "com.nitw2014.mutual.taskprovider";
    Account mAccount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setLayoutManager(new LinearLayoutManager(this));
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_dashboard);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);
        mDrawerContentRight = (RelativeLayout) findViewById(R.id.below_status);

       // RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.below_statusbar);
//        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) mDrawerContentRight.getLayoutParams();
//        lp.height = getStatusBarHeight();
        Log.d("ActivityDashboard", "StatusBar height= " + getStatusBarHeight());
        list=(ListView)findViewById(R.id.list);

        mDrawerLayout = getDrawerLayout();
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        mDrawerList = getDrawerList();

        activityContext = this;
      //  filteringListView = (ListView) findViewById(R.id.list_filtering);
        feedDashboardRecyclerList = new ArrayList<>();
        adapterDashboardRecycler = new AdapterDashboardRecycler(this, feedDashboardRecyclerList);
        dashboardObserver = new DashboardObserver(new Handler());
        Log.d("ActivityDashboard", "Count = "+feedDashboardRecyclerList.size());
//        CustomList adapter = new
//                CustomList(ActivityDashboard.this, web, imageId);
//        list.setAdapter(adapter);
//        setAdapter(adapterDashboardRecycler);

        mAccount = Launcher.createSyncAccount(this);
        swipeRefreshLayoutDashboard = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout_dashboard);
        swipeRefreshLayoutDashboard.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                swipeRefreshLayoutDashboard.setRefreshing(true);
                if(mAccount != null)
                {
                    Log.d("ActivityDashboard","Starting Sync");
                    ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);
                    Bundle settingsBundle = new Bundle();
                    settingsBundle.putBoolean(
                            ContentResolver.SYNC_EXTRAS_MANUAL, true);
                    settingsBundle.putBoolean(
                            ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                    ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);
                }
                new RunUIRefresh().execute();
            }
        });

        fab_dashboard = (FloatingActionButton) findViewById(R.id.dashboard_submittask);
        fab_dashboard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent codeVerifyActivityIntent = new Intent(getApplicationContext(), ActivitySubmitTask.class);
                //codeVerifyActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(codeVerifyActivityIntent);
            }
        });

        getDataFromContentProviderDashboard();

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(getDrawable(R.mipmap.ic_actionbar_logo));

        ItemTouchHelper.Callback callback = new SimpleItemTouchHelperCallback(adapterDashboardRecycler);
        ItemTouchHelper touchHelper = new ItemTouchHelper(callback);
        touchHelper.attachToRecyclerView(getRecyclerView());

//        CustomList adapter = new
//                CustomList(ActivityDashboard.this, web, imageId);

        leftNavigationBar();

        //list.setAdapter(adapter);



        //---------------------------------CODE FOR IMAGE-------------------------------------------
        /*
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQ_CODE_PICK_IMAGE);
        */
        //------------------------------------------------------------------------------------------


    }
    private void leftNavigationBar() {
        HashMap<String, Object> hashList = new HashMap<String, Object>();
        String[] filter_array = getResources().getStringArray(R.array.filtering_array);
        int[] imageId = {
                R.drawable.profile_copy,
                R.drawable.background_design,
                R.drawable.mutualing_center,
                R.drawable.histroy,
                R.drawable.arrow,
                R.drawable.logout_copy};
        filteringList.clear();
// image

        for (int i = 0; i < filter_array.length; i++) {
            hashList = new HashMap<String, Object>();
            hashList.put("image", imageId[i]);
            hashList.put("text", filter_array[i]);
            filteringList.add(hashList);
        }
        leftNavAdapter = new SimpleAdapter(ActivityDashboard.this,
                filteringList, R.layout.filter_row, new String[]
                {"text", "image"},
                new int[]{R.id.filter_name, R.id.imageView5}) {
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                // TODO Auto-generated method stub
                View vChild = super.getView(position, convertView, parent);
                TextView tv = (TextView) vChild.findViewById(R.id.filter_name);
                return vChild;
            }
        };
        list.setAdapter(leftNavAdapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                if (position == 0) {
                    //profilr
                    Intent userIntent = new Intent(ActivityDashboard.this, ActivityFriendProfile.class);
                    ActivityDashboard.this.startActivity(userIntent);
                    mDrawerLayout.closeDrawer(Gravity.END);}

                else if (position == 1) {
                    //sumbit task
                    Intent userIntent = new Intent(ActivityDashboard.this, SubmitTaskDialogFragment.class);
                    ActivityDashboard.this.startActivity(userIntent);
                    mDrawerLayout.closeDrawer(Gravity.END);
                } else if (position == 2) {
                    //mutual center
                    Intent perferenceListIntent = new Intent(ActivityDashboard.this, ActivityMutualCenter.class);
                    startActivity(perferenceListIntent);
                    mDrawerLayout.closeDrawer(Gravity.END);

                } else if (position == 3) {
                    //History
                    Intent findFreindsIntent = new Intent(ActivityDashboard.this, ActivityHistory.class);
                    startActivity(findFreindsIntent);
                    mDrawerLayout.closeDrawer(Gravity.END);
                } else if (position == 4) {
//            //About
//            Intent about = new Intent(ActivityDashboard.this, ActivityDetails.class);
//            startActivity(about);
                    mDrawerLayout.closeDrawer(Gravity.END);
                } else if (position == 5) {
                    //terms and condition
                    Intent about = new Intent(ActivityDashboard.this, ActivityLogin.class);
                    startActivity(about);
                    ActivityDashboard.this.finish();

                    mDrawerLayout.closeDrawer(Gravity.END);
                }
            }
        });    }
    @Override
    protected void onResume() {
        super.onResume();
        getContentResolver().registerContentObserver(URI_TO_OBSERVE, true, dashboardObserver);
//        preferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),MODE_PRIVATE);
//        ((TextView)findViewById(R.id.user_name)).setText( preferences.getString("FirstName", "NaN"));
//        Log.d("name of the profile",preferences.getString("FirstName", "NaN"));
//        Log.d("image of the profile",preferences.getString("ProfPicUri", "NaN"));
//
//        if(!preferences.getString("ProfPicUri","NaN").equals("NaN"))
//            Glide.with(this)
//                    .load(new File(Uri.parse(preferences.getString("ProfPicUri","NaN")).getPath()))
//                    .centerCrop()
//                    .into((CircleImageView)findViewById(R.id.user_image));


    }
//    private void leftNavigationBar() {
//
//    }


    @Override
    protected void onPause() {
        super.onPause();
        getContentResolver().unregisterContentObserver(dashboardObserver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_dashboard, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case R.id.history_actionbar_dashboard:
                Intent historyIntent = new Intent(this, ActivityHistory.class);
                startActivity(historyIntent);
                return true;
            case R.id.navigation_actionbar_dashboard:

                if(mDrawerLayout.isDrawerOpen(Gravity.END))
                    mDrawerLayout.closeDrawer(Gravity.END);
                else
                    mDrawerLayout.openDrawer(Gravity.END);
                return true;
            case R.id.notification_actionbar_dashboard:
                return true;
            case R.id.mutualcenter_actionbar_dashboard:
                Intent mutualcenterIntent = new Intent(this, ActivityMutualCenter.class);
                startActivity(mutualcenterIntent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    protected void getDataFromContentProviderDashboard()
    {
        cursor = getContentResolver().query(URI_TO_OBSERVE, new String[] {ProviderContractTask.TasksAssigned.SUBMITTER, ProviderContractTask.TasksAssigned.TASK}, null, null, ProviderContractTask.TasksAssigned.DEFAULT_SORT_ORDER);

        feedDashboardRecyclerList.clear();
        if(cursor != null)
        {
            while(cursor.moveToNext())
            {
                feedDashboardRecycler = new FeedDashboardRecycler();
                feedDashboardRecycler.setFromTextViewString(cursor.getString(0));
                feedDashboardRecycler.setTaskTextViewString(cursor.getString(1));
                feedDashboardRecyclerList.add(feedDashboardRecycler);
            }
        }
        cursor.close();
    }



    //------------------------------------CLASSES---------------------------------------------------

    class DashboardObserver extends ContentObserver{

        DashboardObserver(Handler handler)
        {
            super(handler);
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);
            new RunUIRefresh().execute();
        }
    }

    private class RunUIRefresh extends AsyncTask<Void, Void, Void>{
        @Override
        protected Void doInBackground(Void... voids) {
            try {
                Thread.sleep(3000);
                getDataFromContentProviderDashboard();
            }catch(InterruptedException ie)
            {
                Log.d("ActivityDashboard", "InterruptedException");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            getAdapter().notifyDataSetChanged();
            swipeRefreshLayoutDashboard.setRefreshing(false);
        }
    }

    interface ItemTouchHelperAdapter{

        void onItemSwipeLeft(int position);

        void onItemSwipeRight(int position);

    }

    private class SimpleItemTouchHelperCallback extends ItemTouchHelper.Callback{

        ItemTouchHelperAdapter mAdapter;
        protected SimpleItemTouchHelperCallback(ItemTouchHelperAdapter adapter)
        {
            mAdapter = adapter;
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            if(direction == ItemTouchHelper.START)
                mAdapter.onItemSwipeLeft(viewHolder.getAdapterPosition());
            else if(direction == ItemTouchHelper.END)
                mAdapter.onItemSwipeRight(viewHolder.getAdapterPosition());

        }

        @Override
        public int getMovementFlags(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

            int dragFlags = ItemTouchHelper.UP | ItemTouchHelper.DOWN;
            int swipeFlags = ItemTouchHelper.START | ItemTouchHelper.END;

            return makeMovementFlags(dragFlags, swipeFlags);
        }

        @Override
        public boolean isItemViewSwipeEnabled() {
            return true;
        }

        @Override
        public boolean isLongPressDragEnabled() {
            return false;
        }
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

    }

    //------------------------------CODE FOR IMAGE--------------------------------------------------
    /*
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            switch (requestCode) {
                case REQ_CODE_PICK_IMAGE:
                    if (resultCode == RESULT_OK) {
                        InputStream is = getContentResolver().openInputStream(data.getData());
                        Bitmap bitmap = BitmapFactory.decodeStream(is);
                        JSONObject jsonObject = new JSONObject().put("UserID", 512).put("UserPhoto", bitmap);
                        new BackgroundTask().execute(jsonObject);
                    }

            }
        }catch(FileNotFoundException fnfe)
        {
            Log.d("ActivityDashboard", "FileNotFoundException");
        }catch (JSONException je)
        {
            Log.d("ActivityDashboard", "JSONException");
        }

    }

    class BackgroundTask extends AsyncTask<JSONObject, Void, Void>
    {

        @Override
        protected Void doInBackground(JSONObject... jsonObjects) {
            String temporaryString = "", inputString = "";
            try {
                JSONObject jsonObject = jsonObjects[0];
                Log.d("Sending JSONObject", jsonObject.toString());
                URL url = new URL("http://www.mutualing.com/insertUserPhoto");
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("POST");
                urlConnection.setDoInput(true);
                urlConnection.setDoOutput(true);
                urlConnection.connect();
                Log.d("ServiceQueryIntent", "Sending JSON object: " + jsonObject.toString());
                OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
                outputStreamWriter.write(jsonObject.toString());
                outputStreamWriter.flush();
                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                Log.d("ServiceQueryIntent", "Reading userid");
                while ((temporaryString = bufferedReader.readLine()) != null) {
                    inputString = inputString + temporaryString;
                }
                Log.d("ServiceQueryIntent", "Read userid: " + inputString);
            }catch(MalformedURLException ex)
            {
                Log.d("AsyncTask", "MalformedURLException");
            }catch(ProtocolException pe)
            {
                Log.d("AsyncTask", "ProtocolException");
            }catch (IOException ioe)
            {
                Log.d("AsyncTask", "IOException: "+ioe.getMessage());
            }
            return null;
        }
    }
    */
    //----------------------------------------------------------------------------------------------

}
