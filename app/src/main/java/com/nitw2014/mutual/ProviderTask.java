package com.nitw2014.mutual;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;

import java.util.HashMap;

public class ProviderTask extends ContentProvider{

    private static final String TAG = "ProviderTask";

    private static final String DATABASE_NAME = "mutualtasks.db";
    private static final int DATABASE_VERSION = 3;
    private static final String TASKS_TABLE_NAME = "tasks";
    private static final String ASKED_TABLE_NAME = "asked";
    private static final String SHARED_TABLE_NAME = "shared";
    private static final String PERFORMED_TABLE_NAME = "performed";
    private static final String FRIENDS_TABLE_NAME = "friends";

    private static HashMap<String, String> sTasksProjectionMap;
    private static HashMap<String, String> sAskedProjectionMap;
    private static HashMap<String, String> sSharedProjectionMap;
    private static HashMap<String, String> sPerformedProjectionMap;
    private static HashMap<String, String> sFriendsProjectionMap;

    private static final int TASKS = 1;
    private static final int TASK_ID = 2;
    private static final int ASKED = 3;
    private static final int ASKED_ID = 4;
    private static final int SHARED = 5;
    private static final int SHARED_ID = 6;
    private static final int PERFORMED = 7;
    private static final int PERFORMED_ID = 8;
    private static final int FRIENDS = 9;
    private static final int FRIEND_ID = 10;

    private static final UriMatcher sUriMatcher;

    private DatabaseHelper mOpenHelper;

    static{

        sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        sUriMatcher.addURI(ProviderContractTask.AUTHORITY, "tasks", TASKS);
        sUriMatcher.addURI(ProviderContractTask.AUTHORITY, "tasks/#", TASK_ID);
        sUriMatcher.addURI(ProviderContractTask.AUTHORITY, "asked", ASKED);
        sUriMatcher.addURI(ProviderContractTask.AUTHORITY, "asked/#", ASKED_ID);
        sUriMatcher.addURI(ProviderContractTask.AUTHORITY, "shared", SHARED);
        sUriMatcher.addURI(ProviderContractTask.AUTHORITY, "shared/#", SHARED_ID);
        sUriMatcher.addURI(ProviderContractTask.AUTHORITY, "performed", PERFORMED);
        sUriMatcher.addURI(ProviderContractTask.AUTHORITY, "performed/#", PERFORMED_ID);
        sUriMatcher.addURI(ProviderContractTask.AUTHORITY, "friends", FRIENDS);
        sUriMatcher.addURI(ProviderContractTask.AUTHORITY, "friends/#", FRIEND_ID);


        sTasksProjectionMap = new HashMap<>();
        sTasksProjectionMap.put(ProviderContractTask.TasksAssigned._ID, ProviderContractTask.TasksAssigned._ID);
        sTasksProjectionMap.put(ProviderContractTask.TasksAssigned.TASK, ProviderContractTask.TasksAssigned.TASK);
        sTasksProjectionMap.put(ProviderContractTask.TasksAssigned.TASK_ID, ProviderContractTask.TasksAssigned.TASK_ID);
        sTasksProjectionMap.put(ProviderContractTask.TasksAssigned.SUBMITTER, ProviderContractTask.TasksAssigned.SUBMITTER);
        sTasksProjectionMap.put(ProviderContractTask.TasksAssigned.SUBMITTER_ID, ProviderContractTask.TasksAssigned.SUBMITTER_ID);

        sAskedProjectionMap = new HashMap<>();
        sAskedProjectionMap.put(ProviderContractTask.AskedHistory._ID, ProviderContractTask.AskedHistory._ID);
        sAskedProjectionMap.put(ProviderContractTask.AskedHistory.TASK, ProviderContractTask.AskedHistory.TASK);
        sAskedProjectionMap.put(ProviderContractTask.AskedHistory.TASK_ID, ProviderContractTask.AskedHistory.TASK_ID);
        sAskedProjectionMap.put(ProviderContractTask.AskedHistory.TASK_ANSWER, ProviderContractTask.AskedHistory.TASK_ANSWER);
        sAskedProjectionMap.put(ProviderContractTask.AskedHistory.SUBMITTER, ProviderContractTask.AskedHistory.SUBMITTER);
        sAskedProjectionMap.put(ProviderContractTask.AskedHistory.SUBMITTER_ID, ProviderContractTask.AskedHistory.SUBMITTER_ID);

        sPerformedProjectionMap = new HashMap<>();
        sPerformedProjectionMap.put(ProviderContractTask.PerformedHistory._ID, ProviderContractTask.PerformedHistory._ID);
        sPerformedProjectionMap.put(ProviderContractTask.PerformedHistory.TASK, ProviderContractTask.PerformedHistory.TASK);
        sPerformedProjectionMap.put(ProviderContractTask.PerformedHistory.TASK_ID, ProviderContractTask.PerformedHistory.TASK_ID);
        sPerformedProjectionMap.put(ProviderContractTask.PerformedHistory.ANSWER, ProviderContractTask.PerformedHistory.ANSWER);
        sPerformedProjectionMap.put(ProviderContractTask.PerformedHistory.SUBMITTER, ProviderContractTask.PerformedHistory.SUBMITTER);
        sPerformedProjectionMap.put(ProviderContractTask.PerformedHistory.SUBMITTER_ID, ProviderContractTask.PerformedHistory.SUBMITTER_ID);

        sSharedProjectionMap = new HashMap<>();
        sSharedProjectionMap.put(ProviderContractTask.SharedHistory._ID, ProviderContractTask.SharedHistory._ID);
        sSharedProjectionMap.put(ProviderContractTask.SharedHistory.TASK, ProviderContractTask.SharedHistory.TASK);
        sSharedProjectionMap.put(ProviderContractTask.SharedHistory.TASK_ID, ProviderContractTask.SharedHistory.TASK_ID);
        sSharedProjectionMap.put(ProviderContractTask.SharedHistory.SUBMITTER, ProviderContractTask.SharedHistory.SUBMITTER);
        sSharedProjectionMap.put(ProviderContractTask.SharedHistory.SUBMITTER_ID, ProviderContractTask.SharedHistory.SUBMITTER_ID);

        sFriendsProjectionMap = new HashMap<>();
        sFriendsProjectionMap.put(ProviderContractTask.FriendsList._ID, ProviderContractTask.FriendsList._ID);
        sFriendsProjectionMap.put(ProviderContractTask.FriendsList.FRIEND, ProviderContractTask.FriendsList.FRIEND);
        sFriendsProjectionMap.put(ProviderContractTask.FriendsList.FRIEND_ID, ProviderContractTask.FriendsList.FRIEND_ID);

    }

    private static class DatabaseHelper extends SQLiteOpenHelper{

        public DatabaseHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        void createFriendsTable(SQLiteDatabase sqLiteDatabase)
        {
            sqLiteDatabase.execSQL("CREATE TABLE " + FRIENDS_TABLE_NAME + " ("
                    + ProviderContractTask.FriendsList._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ProviderContractTask.FriendsList.FRIEND + " TEXT, "
                    + ProviderContractTask.FriendsList.FRIEND_ID + " TEXT"
                    + ");");
        }

        @Override
        public void onCreate(SQLiteDatabase sqLiteDatabase) {

            createFriendsTable(sqLiteDatabase);

            sqLiteDatabase.execSQL("CREATE TABLE " + TASKS_TABLE_NAME + " ("
                    + ProviderContractTask.TasksAssigned._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ProviderContractTask.TasksAssigned.TASK + " TEXT, "
                    + ProviderContractTask.TasksAssigned.TASK_ID + " TEXT, "
                    + ProviderContractTask.TasksAssigned.SUBMITTER + " TEXT, "
                    + ProviderContractTask.TasksAssigned.SUBMITTER_ID + " TEXT"
                    + ");");

            sqLiteDatabase.execSQL("CREATE TABLE " + ASKED_TABLE_NAME + " ("
                    + ProviderContractTask.AskedHistory._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ProviderContractTask.AskedHistory.TASK + " TEXT, "
                    + ProviderContractTask.AskedHistory.TASK_ID + " TEXT, "
                    + ProviderContractTask.AskedHistory.TASK_ANSWER + " TEXT, "
                    + ProviderContractTask.AskedHistory.SUBMITTER + " TEXT, "
                    + ProviderContractTask.AskedHistory.SUBMITTER_ID + " TEXT"
                    + ");");

            sqLiteDatabase.execSQL("CREATE TABLE " + PERFORMED_TABLE_NAME + " ("
                    + ProviderContractTask.PerformedHistory._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ProviderContractTask.PerformedHistory.TASK + " TEXT, "
                    + ProviderContractTask.PerformedHistory.TASK_ID + " TEXT, "
                    + ProviderContractTask.PerformedHistory.ANSWER + " TEXT, "
                    + ProviderContractTask.PerformedHistory.SUBMITTER + " TEXT, "
                    + ProviderContractTask.PerformedHistory.SUBMITTER_ID + " TEXT"
                    + ");");

            sqLiteDatabase.execSQL("CREATE TABLE " + SHARED_TABLE_NAME + " ("
                    + ProviderContractTask.SharedHistory._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + ProviderContractTask.SharedHistory.TASK + " TEXT, "
                    + ProviderContractTask.SharedHistory.TASK_ID + " TEXT, "
                    + ProviderContractTask.SharedHistory.SUBMITTER + " TEXT, "
                    + ProviderContractTask.SharedHistory.SUBMITTER_ID + " TEXT"
                    + ");");

        }

        @Override
        public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion){

            if(newVersion == 3 && oldVersion == 2)
                createFriendsTable(sqLiteDatabase);
            else {
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TASKS_TABLE_NAME);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + ASKED_TABLE_NAME);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + PERFORMED_TABLE_NAME);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + SHARED_TABLE_NAME);
                sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + SHARED_TABLE_NAME);
                onCreate(sqLiteDatabase);
            }

        }

    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch(sUriMatcher.match(uri)){
            case TASKS: return ProviderContractTask.TasksAssigned.CONTENT_TYPE;
            case TASK_ID: return ProviderContractTask.TasksAssigned.CONTENT_ITEM_TYPE;
            case ASKED: return ProviderContractTask.AskedHistory.CONTENT_TYPE;
            case ASKED_ID: return ProviderContractTask.AskedHistory.CONTENT_ITEM_TYPE;
            case SHARED: return ProviderContractTask.SharedHistory.CONTENT_TYPE;
            case SHARED_ID: return ProviderContractTask.SharedHistory.CONTENT_ITEM_TYPE;
            case PERFORMED: return ProviderContractTask.PerformedHistory.CONTENT_TYPE;
            case PERFORMED_ID: return ProviderContractTask.PerformedHistory.CONTENT_ITEM_TYPE;
            case  FRIENDS: return ProviderContractTask.FriendsList.CONTENT_TYPE;
            case FRIEND_ID: return ProviderContractTask.FriendsList.CONTENT_ITEM_TYPE;
            default:
                Log.d("ProviderTask","UriMatcher-getType: "+sUriMatcher.match(uri));
                throw new IllegalArgumentException("Unknown URI " + uri);
        }
    }

    @Override
    public boolean onCreate() {
        mOpenHelper = new DatabaseHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        SQLiteQueryBuilder queryBuilderTasks = new SQLiteQueryBuilder();
        SQLiteQueryBuilder queryBuilderAsked = new SQLiteQueryBuilder();
        SQLiteQueryBuilder queryBuilderPerformed = new SQLiteQueryBuilder();
        SQLiteQueryBuilder queryBuilderShared = new SQLiteQueryBuilder();
        SQLiteQueryBuilder queryBuilderFriends = new SQLiteQueryBuilder();
        String orderByTasks, orderByAsked, orderByPerformed, orderByShared, orderByFriends;

        queryBuilderTasks.setTables(TASKS_TABLE_NAME);
        queryBuilderAsked.setTables(ASKED_TABLE_NAME);
        queryBuilderPerformed.setTables(PERFORMED_TABLE_NAME);
        queryBuilderShared.setTables(SHARED_TABLE_NAME);
        queryBuilderFriends.setTables(FRIENDS_TABLE_NAME);

        Cursor c;
        SQLiteDatabase sqLiteDatabase = mOpenHelper.getReadableDatabase();

        if((TextUtils.isEmpty(sortOrder))) {
            orderByTasks = ProviderContractTask.TasksAssigned.DEFAULT_SORT_ORDER;
            orderByAsked = ProviderContractTask.AskedHistory.DEFAULT_SORT_ORDER;
            orderByPerformed = ProviderContractTask.PerformedHistory.DEFAULT_SORT_ORDER;
            orderByShared = ProviderContractTask.SharedHistory.DEFAULT_SORT_ORDER;
            orderByFriends = ProviderContractTask.FriendsList.DEFAULT_SORT_ORDER;
        }
        else {
            orderByTasks = sortOrder;
            orderByAsked = sortOrder;
            orderByPerformed = sortOrder;
            orderByShared = sortOrder;
            orderByFriends = sortOrder;
        }

        switch(sUriMatcher.match(uri)){
            case TASKS:
                queryBuilderTasks.setProjectionMap(sTasksProjectionMap);
                c = queryBuilderTasks.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, orderByTasks);
                break;
            case TASK_ID:
                queryBuilderTasks.setProjectionMap(sTasksProjectionMap);
                queryBuilderTasks.appendWhere(ProviderContractTask.TasksAssigned._ID + "=" + uri.getPathSegments().get(1));
                c = queryBuilderTasks.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, orderByTasks);
                break;
            case ASKED:
                queryBuilderAsked.setProjectionMap(sAskedProjectionMap);
                c = queryBuilderAsked.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, orderByAsked);
                break;
            case ASKED_ID:
                queryBuilderAsked.setProjectionMap(sAskedProjectionMap);
                queryBuilderAsked.appendWhere(ProviderContractTask.AskedHistory._ID + "=" + uri.getPathSegments().get(1));
                c = queryBuilderAsked.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, orderByAsked);
                break;
            case PERFORMED:
                queryBuilderAsked.setProjectionMap(sPerformedProjectionMap);
                c = queryBuilderPerformed.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, orderByPerformed);
                break;
            case PERFORMED_ID:
                queryBuilderAsked.setProjectionMap(sPerformedProjectionMap);
                queryBuilderAsked.appendWhere(ProviderContractTask.PerformedHistory._ID + "=" + uri.getPathSegments().get(1));
                c = queryBuilderPerformed.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, orderByPerformed);
                break;
            case SHARED:
                queryBuilderAsked.setProjectionMap(sSharedProjectionMap);
                c = queryBuilderShared.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, orderByShared);
                break;
            case SHARED_ID:
                queryBuilderAsked.setProjectionMap(sSharedProjectionMap);
                queryBuilderAsked.appendWhere(ProviderContractTask.SharedHistory._ID + "=" + uri.getPathSegments().get(1));
                c = queryBuilderShared.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, orderByShared);
                break;
            case FRIENDS:
                queryBuilderFriends.setProjectionMap(sFriendsProjectionMap);
                c = queryBuilderFriends.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, orderByFriends);
                break;
            case FRIEND_ID:
                queryBuilderAsked.setProjectionMap(sFriendsProjectionMap);
                queryBuilderAsked.appendWhere(ProviderContractTask.FriendsList._ID + "=" + uri.getPathSegments().get(1));
                c = queryBuilderFriends.query(sqLiteDatabase, projection, selection, selectionArgs, null, null, orderByFriends);
            default: {
                Log.d("ProviderTask","UriMatcher-query: "+sUriMatcher.match(uri));
                throw new IllegalArgumentException("Unknown URI " + uri);
            }
        }

        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues initialValues) {
        if(sUriMatcher.match(uri) != TASKS && sUriMatcher.match(uri) != ASKED && sUriMatcher.match(uri) != PERFORMED && sUriMatcher.match(uri) != SHARED && sUriMatcher.match(uri) != FRIENDS){
            Log.d("ProviderTask","UriMatcher-insert: "+sUriMatcher.match(uri));
            throw new IllegalArgumentException("Unknown URI " + uri);
        }

        ContentValues values;
        if(initialValues != null){
            values = new ContentValues(initialValues);
        }else{
            values = new ContentValues();
        }

        switch(sUriMatcher.match(uri)){
            case FRIENDS:
            {
                if(!values.containsKey(ProviderContractTask.FriendsList.FRIEND)){
                    values.put(ProviderContractTask.FriendsList.FRIEND, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.FriendsList.FRIEND_ID)){
                    values.put(ProviderContractTask.FriendsList.FRIEND_ID, "NaN");
                }

                SQLiteDatabase sqLiteDatabase = mOpenHelper.getWritableDatabase();
                long rowId = sqLiteDatabase.insert(FRIENDS_TABLE_NAME, ProviderContractTask.FriendsList.FRIEND, values);
                if(rowId > 0){
                    Uri friendUri = ContentUris.withAppendedId(ProviderContractTask.FriendsList.CONTENT_URI, rowId);
                    getContext().getContentResolver().notifyChange(friendUri, null);
                    Log.d("ProviderTask", "Successfully inserted data " + initialValues.toString());
                    return friendUri;
                }

            }break;
            case TASKS:
            {
                if(!values.containsKey(ProviderContractTask.TasksAssigned.TASK)){
                    values.put(ProviderContractTask.TasksAssigned.TASK, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.TasksAssigned.TASK_ID)){
                    values.put(ProviderContractTask.TasksAssigned.TASK_ID, "-1");
                }

                if(!values.containsKey(ProviderContractTask.TasksAssigned.SUBMITTER)){
                    values.put(ProviderContractTask.TasksAssigned.SUBMITTER, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.TasksAssigned.SUBMITTER_ID)){
                    values.put(ProviderContractTask.TasksAssigned.SUBMITTER_ID, "-1");
                }

                SQLiteDatabase sqLiteDatabase = mOpenHelper.getWritableDatabase();
                long rowId = sqLiteDatabase.insert(TASKS_TABLE_NAME, ProviderContractTask.TasksAssigned.TASK, values);
                if(rowId > 0){
                    Uri taskUri = ContentUris.withAppendedId(ProviderContractTask.TasksAssigned.CONTENT_URI, rowId);
                    getContext().getContentResolver().notifyChange(taskUri, null);
                    Log.d("ProviderTask", "Successfully inserted data " + initialValues.toString());
                    return taskUri;
                }
            }
                break;
            case ASKED:
            {
                if(!values.containsKey(ProviderContractTask.AskedHistory.TASK)){
                    values.put(ProviderContractTask.AskedHistory.TASK, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.AskedHistory.TASK_ID)){
                    values.put(ProviderContractTask.AskedHistory.TASK_ID, "-1");
                }

                if(!values.containsKey(ProviderContractTask.AskedHistory.TASK_ANSWER)){
                    values.put(ProviderContractTask.AskedHistory.TASK_ANSWER, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.AskedHistory.SUBMITTER)){
                    values.put(ProviderContractTask.AskedHistory.SUBMITTER, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.AskedHistory.SUBMITTER_ID)){
                    values.put(ProviderContractTask.AskedHistory.SUBMITTER_ID, "-1");
                }

                SQLiteDatabase sqLiteDatabase = mOpenHelper.getWritableDatabase();
                long rowId = sqLiteDatabase.insert(ASKED_TABLE_NAME, ProviderContractTask.AskedHistory.TASK, values);
                if(rowId > 0){
                    Uri taskUri = ContentUris.withAppendedId(ProviderContractTask.AskedHistory.CONTENT_URI, rowId);
                    getContext().getContentResolver().notifyChange(taskUri, null);
                    Log.d("ProviderTask", "Successfully inserted data " + initialValues.toString());
                    return taskUri;
                }
            }
                break;
            case PERFORMED:
            {
                if(!values.containsKey(ProviderContractTask.PerformedHistory.TASK)){
                    values.put(ProviderContractTask.PerformedHistory.TASK, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.PerformedHistory.TASK_ID)){
                    values.put(ProviderContractTask.PerformedHistory.TASK_ID, "-1");
                }

                if(!values.containsKey(ProviderContractTask.PerformedHistory.ANSWER)){
                    values.put(ProviderContractTask.PerformedHistory.ANSWER, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.PerformedHistory.SUBMITTER)){
                    values.put(ProviderContractTask.PerformedHistory.SUBMITTER, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.PerformedHistory.SUBMITTER_ID)){
                    values.put(ProviderContractTask.PerformedHistory.SUBMITTER_ID, "-1");
                }

                SQLiteDatabase sqLiteDatabase = mOpenHelper.getWritableDatabase();
                long rowId = sqLiteDatabase.insert(PERFORMED_TABLE_NAME, ProviderContractTask.PerformedHistory.TASK, values);
                if(rowId > 0){
                    Uri taskUri = ContentUris.withAppendedId(ProviderContractTask.PerformedHistory.CONTENT_URI, rowId);
                    getContext().getContentResolver().notifyChange(taskUri, null);
                    Log.d("ProviderTask", "Successfully inserted data " + initialValues.toString());
                    return taskUri;
                }
            }
                break;
            case SHARED:
            {
                if(!values.containsKey(ProviderContractTask.SharedHistory.TASK)){
                    values.put(ProviderContractTask.SharedHistory.TASK, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.SharedHistory.TASK_ID)){
                    values.put(ProviderContractTask.SharedHistory.TASK_ID, "-1");
                }

                if(!values.containsKey(ProviderContractTask.SharedHistory.SUBMITTER)){
                    values.put(ProviderContractTask.SharedHistory.SUBMITTER, "NaN");
                }

                if(!values.containsKey(ProviderContractTask.SharedHistory.SUBMITTER_ID)){
                    values.put(ProviderContractTask.SharedHistory.SUBMITTER_ID, "-1");
                }

                SQLiteDatabase sqLiteDatabase = mOpenHelper.getWritableDatabase();
                long rowId = sqLiteDatabase.insert(SHARED_TABLE_NAME, ProviderContractTask.SharedHistory.TASK, values);
                if(rowId > 0){
                    Uri taskUri = ContentUris.withAppendedId(ProviderContractTask.SharedHistory.CONTENT_URI, rowId);
                    getContext().getContentResolver().notifyChange(taskUri, null);
                    Log.d("ProviderTask", "Successfully inserted data " + initialValues.toString());
                    return taskUri;
                }
            }
                break;
            default: throw new IllegalArgumentException("Failed to insert row into " + uri);
        }

        throw new IllegalArgumentException("Failed to insert row into " + uri);

    }

    @Override
    public int update(Uri uri, ContentValues values, String where, String[] whereArgs) {
        SQLiteDatabase sqLiteDatabase = mOpenHelper.getWritableDatabase();
        int count;
        String taskId = uri.getPathSegments().get(1);
        switch(sUriMatcher.match(uri)){
            case TASKS:
                count = sqLiteDatabase.update(TASKS_TABLE_NAME, values, where, whereArgs);
                break;

            case TASK_ID:
                count = sqLiteDatabase.update(TASKS_TABLE_NAME, values, ProviderContractTask.TasksAssigned._ID + "=" + taskId + (!TextUtils.isEmpty(where)?"AND (" + where + ")":""), whereArgs);
                break;

            case ASKED:
                count = sqLiteDatabase.update(ASKED_TABLE_NAME, values, where, whereArgs);
                break;

            case ASKED_ID:
                count = sqLiteDatabase.update(ASKED_TABLE_NAME, values, ProviderContractTask.AskedHistory._ID + "=" + taskId + (!TextUtils.isEmpty(where)?"AND (" + where + ")":""), whereArgs);
                break;

            case PERFORMED:
                count = sqLiteDatabase.update(PERFORMED_TABLE_NAME, values, where, whereArgs);
                break;

            case PERFORMED_ID:
                count = sqLiteDatabase.update(PERFORMED_TABLE_NAME, values, ProviderContractTask.PerformedHistory._ID + "=" + taskId + (!TextUtils.isEmpty(where)?"AND (" + where + ")":""), whereArgs);
                break;

            case SHARED:
                count = sqLiteDatabase.update(SHARED_TABLE_NAME, values, where, whereArgs);
                break;

            case SHARED_ID:
                count = sqLiteDatabase.update(SHARED_TABLE_NAME, values, ProviderContractTask.SharedHistory._ID + "=" + taskId + (!TextUtils.isEmpty(where)?"AND (" + where + ")":""), whereArgs);
                break;

            case FRIENDS:
                count = sqLiteDatabase.update(FRIENDS_TABLE_NAME, values, where, whereArgs);
                break;

            case FRIEND_ID:
                count = sqLiteDatabase.update(FRIENDS_TABLE_NAME, values, ProviderContractTask.FriendsList._ID + "=" + taskId + (!TextUtils.isEmpty(where)?"AND (" + where + ")":""), whereArgs);
                break;

            default: {
                Log.d("ProviderTask","UriMatcher-update: "+sUriMatcher.match(uri));
                throw new IllegalArgumentException("Unknown URI " + uri);
            }
        }
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }

    @Override
    public int delete(Uri uri, String where, String[] whereArgs) {
        SQLiteDatabase sqLiteDatabase = mOpenHelper.getWritableDatabase();
        int count;
        String taskId = uri.getPathSegments().get(1);

        Log.d("ProviderTask","Delete Query Starting...");

        switch(sUriMatcher.match(uri)){
            case TASKS:
                count = sqLiteDatabase.delete(TASKS_TABLE_NAME, where, whereArgs);
                break;
            case TASK_ID:
                count = sqLiteDatabase.delete(TASKS_TABLE_NAME, ProviderContractTask.TasksAssigned._ID + "=" + taskId + (!TextUtils.isEmpty(where) ? " AND (" + where + ")" : ""), whereArgs);
                break;
            case ASKED:
                count = sqLiteDatabase.delete(ASKED_TABLE_NAME, where, whereArgs);
                break;
            case ASKED_ID:
                count = sqLiteDatabase.delete(ASKED_TABLE_NAME, ProviderContractTask.AskedHistory._ID + "=" + taskId + (!TextUtils.isEmpty(where) ? " AND (" + where + ")" : ""), whereArgs);
                break;
            case PERFORMED:
                count = sqLiteDatabase.delete(PERFORMED_TABLE_NAME, where, whereArgs);
                break;
            case PERFORMED_ID:
                count = sqLiteDatabase.delete(PERFORMED_TABLE_NAME, ProviderContractTask.PerformedHistory._ID + "=" + taskId + (!TextUtils.isEmpty(where) ? " AND (" + where + ")" : ""), whereArgs);
                break;
            case SHARED:
                count = sqLiteDatabase.delete(SHARED_TABLE_NAME, where, whereArgs);
                break;
            case SHARED_ID:
                count = sqLiteDatabase.delete(SHARED_TABLE_NAME, ProviderContractTask.SharedHistory._ID + "=" + taskId + (!TextUtils.isEmpty(where) ? " AND (" + where + ")" : ""), whereArgs);
                break;
            case FRIENDS:
                count = sqLiteDatabase.delete(FRIENDS_TABLE_NAME, where, whereArgs);
                break;
            case FRIEND_ID:
                count = sqLiteDatabase.delete(FRIENDS_TABLE_NAME, ProviderContractTask.FriendsList._ID + "=" + taskId + (!TextUtils.isEmpty(where) ? " AND (" + where + ")" : ""), whereArgs);
                break;
            default: {
                Log.d("ProviderTask","UriMatcher-delete: "+sUriMatcher.match(uri));
                throw new IllegalArgumentException("Unknown URI " + uri);
            }
        }
        Log.d("ProviderTask","Deleted the rows");
        getContext().getContentResolver().notifyChange(uri, null);
        return count;
    }
}
