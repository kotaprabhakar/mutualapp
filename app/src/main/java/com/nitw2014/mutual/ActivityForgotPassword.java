package com.nitw2014.mutual;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

public class ActivityForgotPassword extends AppCompatActivity {
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */

    EditText emailID;
    String emailID_string;
    ProgressDialog forgotProgressDialog;
    BroadcastReceiver Successfullysent = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String Signup1 = intent.getStringExtra("Successfullysent");
            if(Signup1.equals("true")){
                forgotProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please check your email for code!", Toast.LENGTH_LONG).show();
                Intent codeVerifyActivityIntent = new Intent(getApplicationContext(), ActivityCodeVerify.class);
                //codeVerifyActivityIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(codeVerifyActivityIntent);
                //finish();
            }
            else{
                forgotProgressDialog.dismiss();
                Toast.makeText(getApplicationContext(), "Please connect to a network and try again!", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(Successfullysent);
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar mutualToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mutualToolbar);
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),MODE_PRIVATE);
        final SharedPreferences.Editor editor = sharedPreferences.edit();

        LocalBroadcastManager.getInstance(this).registerReceiver(Successfullysent, new IntentFilter("SentEmail"));

        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        //FacebookSdk.sdkInitialize(getApplicationContext());
        //callbackManager = CallbackManager.Factory.create();
        //AppEventsLogger.activateApp(getApplication());
        setContentView(R.layout.activity_forgot_password);
        Button button = (Button) findViewById(R.id.SubmitForgot);
        emailID = (EditText)findViewById(R.id.emailIDForgot);

        assert button != null;
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                emailID_string = emailID.getText().toString();
                Intent queryServiceIntent = new Intent(getApplicationContext(), ServiceQueryIntent.class);
                queryServiceIntent.setAction("com.nitw2014.action.FORGOT_PASSWORD");
                queryServiceIntent.putExtra("EmailID", emailID_string);
                editor.putString("EmailID", emailID_string);
                editor.apply();
                //REMOVE FOLLOWING CODE IMMEDIATELY ----START
                //Intent mainActivityIntent = new Intent(getApplicationContext(), ActivityDashboard.class);
                //REMOVE FOLLOWING CODE IMMEDIATELY ----END
                startService(queryServiceIntent);
                forgotProgressDialog = new ProgressDialog(ActivityForgotPassword.this);
                forgotProgressDialog.setMessage("Trying to Send Email!");
                forgotProgressDialog.show();
                //startActivity(mainActivityIntent);
            }
        });

    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}
