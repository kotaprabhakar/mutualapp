package com.nitw2014.mutual;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

/**
 * Created by prabhakar on 25/03/17.
 */

public class AdapterSlideIntro extends FragmentPagerAdapter{

    public AdapterSlideIntro(FragmentManager fm)
    {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Log.d("AdapterSlideIntro","Getting Item at position: " + position);
        return FragmentSlideIntro.newInstance(position);
    }

    @Override
    public int getCount() {
        return 3;
    }
}
