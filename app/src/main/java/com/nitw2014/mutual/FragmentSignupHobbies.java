package com.nitw2014.mutual;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sai Karthik on 6/12/2016.
 */
public class FragmentSignupHobbies extends Fragment {

    FragmentActivity hobbiesFragmentContext;
    Button shareApp, button9;
    EditText hobbiesEditText;
    String tempText;
    AdapterSignupHobbies adapterSignupHobbies;
    final ArrayList<String> hobbies = new ArrayList<>();

    class AdapterSignupHobbies extends ArrayAdapter<String> {
        Context context;
        int layoutResourceId;
        ArrayList<String> hobbiesList;
        AdapterSignupHobbies adapterSignupHobbies;

        AdapterSignupHobbies(Context context, int layoutResourceId, ArrayList<String> hobbiesList){
            super(context, layoutResourceId, hobbiesList);
            this.context = context;
            this.layoutResourceId = layoutResourceId;
            this.hobbiesList = hobbiesList;
            adapterSignupHobbies = this;
        }

        @NonNull
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            if(convertView == null){
                LayoutInflater inflater = ((Activity)context).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }

            String textToDisplay = hobbiesList.get(position);
            TextView ticketText = (TextView)convertView.findViewById(R.id.ticket_text);
            Button closeButton = (Button) convertView.findViewById(R.id.delete_ticket);
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hobbiesList.remove(position);
                    adapterSignupHobbies.notifyDataSetChanged();
                }
            });
            ticketText.setText(textToDisplay);
            return convertView;
        }
    }

    @Override
    public void onAttach(Context context) {
        hobbiesFragmentContext = (FragmentActivity)context;
        super.onAttach(context);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup_hobbies,container,false);
        hobbiesFragmentContext.setTitle("Hobbies");
        final ListView dynamicLinearLayout = (ListView) view.findViewById(R.id.dynamic_linear_hobbies);
        button9 = (Button) view.findViewById(R.id.button9);
        shareApp = (Button)view.findViewById(R.id.shareApp);
        hobbiesEditText = (EditText)view.findViewById(R.id.hobbies_edit_text);
        adapterSignupHobbies = new AdapterSignupHobbies(hobbiesFragmentContext, R.layout.ticket_layout, hobbies);
        dynamicLinearLayout.setAdapter(adapterSignupHobbies);

        button9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tempText = hobbiesEditText.getText().toString();
                if((!tempText.equals("")) && tempText != null) {
                    hobbies.add(tempText);
                }
                hobbiesEditText.setText("");
                adapterSignupHobbies.notifyDataSetChanged();
            }
        });
        shareApp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent queryServiceIntent = new Intent(hobbiesFragmentContext, ServiceQueryIntent.class);
                queryServiceIntent.setAction("com.nitw2014.mutual.action.SET_HOBBIES");
                queryServiceIntent.putExtra("Hobbies",hobbies);
                hobbiesFragmentContext.startService(queryServiceIntent);
            }
        });

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((ActivityDetails)getActivity()).setTitleView("Enter your hobbies!");
    }

    @Override
    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        return super.onCreateAnimation(transit, enter, nextAnim);
    }
}
