package com.nitw2014.mutual;

import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.widget.LinearLayout;
import android.widget.ListView;

/**
 * Created by prabhakar on 06/06/17.
 */

public class RecyclerViewActivity extends AppCompatActivity {

    private RecyclerView rv = null;
    private DrawerLayout mDrawerLayout;
    private LinearLayout mDrawerList;

    public void setAdapter(RecyclerView.Adapter adapter)
    {
        getRecyclerView().setAdapter(adapter);
    }

    public RecyclerView.Adapter getAdapter()
    {
        return getRecyclerView().getAdapter();
    }

    public void setLayoutManager(RecyclerView.LayoutManager mgr)
    {
        getRecyclerView().setLayoutManager(mgr);
    }

    public LinearLayout getDrawerList()
    {
        return mDrawerList;
    }

    public DrawerLayout getDrawerLayout()
    {
        return mDrawerLayout;
    }

    public RecyclerView getRecyclerView()
    {
        if(rv == null)
        {
            setContentView(R.layout.activity_recycler_view);
            rv = (RecyclerView) findViewById(R.id.dashboard_recycler_view);
            mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
            mDrawerList = (LinearLayout) findViewById(R.id.drawer_list);
            rv.setBackgroundColor(getResources().getColor(R.color.whitesmoke));
            rv.setHasFixedSize(true);
        }

        return rv;

    }

}
