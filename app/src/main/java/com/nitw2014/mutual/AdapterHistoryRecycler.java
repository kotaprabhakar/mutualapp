package com.nitw2014.mutual;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by prabhakar on 27/08/16.
 */
class AdapterHistoryRecycler extends RecyclerView.Adapter<AdapterHistoryRecycler.CustomViewHolder>{

    private List<FeedHistoryRecycler> feedHistoryRecyclers;
    private Context applicationContext;

    AdapterHistoryRecycler(Context context, List<FeedHistoryRecycler> feedHistoryRecyclers){
        this.feedHistoryRecyclers = feedHistoryRecyclers;
        this.applicationContext = context;
    }

    @Override
    public int getItemCount() {
        return (null != feedHistoryRecyclers ? feedHistoryRecyclers.size():0);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_history, parent, false);
        return new CustomViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterHistoryRecycler.CustomViewHolder holder, int position) {
        FeedHistoryRecycler feedHistoryRecycler = feedHistoryRecyclers.get(position);
        holder.fromUserView.setText(feedHistoryRecycler.getFromUserString());
        holder.mainTaskView.setText(feedHistoryRecycler.getMainTaskString());
    }

    //Classes

    class CustomViewHolder extends RecyclerView.ViewHolder{

        TextView fromUserView;
        TextView mainTaskView;

        CustomViewHolder(View view){
            super(view);
            this.fromUserView = (TextView)view.findViewById(R.id.fromUser);
            this.mainTaskView = (TextView)view.findViewById(R.id.main_task);
        }

    }



}
