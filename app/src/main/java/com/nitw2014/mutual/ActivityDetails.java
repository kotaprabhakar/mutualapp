package com.nitw2014.mutual;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.io.Serializable;

public class ActivityDetails extends AppCompatActivity implements Serializable{

    TextView titleView;
    ViewPager viewPagerForSignup;

    private final BroadcastReceiver receiverFinish = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("ActivityDetails","ActivityDetails activity finished!");
            finish();
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_details);

        registerReceiver(receiverFinish, new IntentFilter("closeActivity"));

        titleView = (TextView) findViewById(R.id.textView_title);

        Log.d("ActivityDetails", "TitleView is null: " + (titleView == null));
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        viewPagerForSignup = (ViewPager) findViewById(R.id.view_pager_for_signup_details);

        AdapterSignupDetails adapterSignupDetails = new AdapterSignupDetails(getSupportFragmentManager(), this, viewPagerForSignup);
        viewPagerForSignup.setAdapter(adapterSignupDetails);
    }

    public void setTitleView(String titleToSet)
    {
        Log.d("ActivityDetails", "TitleView is null: " + (titleView == null) + " String to set: " + titleToSet);
        titleView.setText(titleToSet);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiverFinish);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_home, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case R.id.home_menu:
                Intent historyIntent = new Intent(this, ActivityDashboard.class);
                startActivity(historyIntent);
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

}

