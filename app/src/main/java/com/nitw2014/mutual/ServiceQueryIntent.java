package com.nitw2014.mutual;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * helper methods.
 */
public class ServiceQueryIntent extends IntentService {
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    public static final String QUERY_SET_DETAILS = "com.nitw2014.mutual.action.SET_DETAILS";
    public static final String QUERY_SET_PLACES = "com.nitw2014.mutual.action.SET_PLACES";
    public static final String QUERY_SET_EDUCATION = "com.nitw2014.mutual.action.SET_EDUCATION";
    public static final String QUERY_SET_CAREER = "com.nitw2014.mutual.action.SET_CAREER";
    public static final String QUERY_SET_HOBBIES = "com.nitw2014.mutual.action.SET_HOBBIES";
    public static final String QUERY_DASHBOARD_DATA = "com.nitw2014.mutual.action.MAIN_DATA";
    public static final String QUERY_INSERT_TASK = "com.nitw2014.mutual.action.INSERT_TASK";
    public static final String QUERY_GET_TASK = "com.nitw2014.mutual.action.GET_TASK";
    public static final String QUERY_GET_SHARED_TASK = "com.nitw2014.mutual.action.GET_SHARED_TASK";
    public static final String QUERY_LOGIN = "com.nitw2014.mutual.action.LOGIN";
    public static final String QUERY_ASSIGN_MORE_USERS = "com.nitw2014.mutual.action.ASSIGN_MORE_USERS";
    public static final String QUERY_INSERT_ANSWER = "com.nitw2014.mutual.action.INSERT_ANSWER";
    public static final String QUERY_GET_USER_COMPLETE_DETAILS = "com.nitw2014.mutual.action.GET_USER_COMPLETE_DETAILS";
    public static final String QUERY_ADD_FRIEND = "com.nitw2014.mutual.action.ADD_FRIEND";
    public static final String QUERY_SEARCH_USER = "com.nitw2014.action.SEARCH_USER";
    public static final String QUERY_GET_FRIEND_LIST = "com.nitw2014.action.GET_FRIEND_LIST";
    public static final String QUERY_INSERT_PROFILE_PIC = "com.nitw2014.action.INSERT_PROFILE_PIC";
    public static final String QUERY_GLOBAL_SEARCH = "com.nitw2014.action.GLOBAL_SEARCH";
    public static final String QUERY_FORGOT_PASSWORD = "com.nitw2014.action.FORGOT_PASSWORD";
    public static final String QUERY_VERIFY_CHANGE_PASSWORD = "com.nitw2014.action.VERIFY_CHANGE_PASSWORD";
    public static final String QUERY_DEACTIVATE_ACCOUNT = "com.nitw2014.action.DEACTIVATE_ACCOUNT";
    public static final String QUERY_VERIFY_DEACTIVATE_ACCOUNT = "com.nitw2014.action.VERIFY_DEACTIVATE_ACCOUNT";
    public static final String URL_SIGNUP_DETAILS = "http://www.mutualing.com/uservote/admin_edit/insertUserData";
    public static final String URL_SIGNUP_PLACES = "http://www.mutualing.com/uservote/admin_edit/insertPlaces";
    public static final String URL_SIGNUP_EDUCATION = "http://www.mutualing.com/uservote/admin_edit/insertEducation";
    public static final String URL_SIGNUP_CAREER = "http://www.mutualing.com/uservote/admin_edit/insertCarriers";
    public static final String URL_SIGNUP_HOBBIES = "http://www.mutualing.com/uservote/admin_edit/insertHobbies";
    public static final String URL_DASHBOARD_DATA = "http://www.mutualing.com/uservote/admin_edit/getAssignedTask";
    public static final String URL_INSERT_TASK = "http://www.mutualing.com/uservote/admin_edit/insertTask";
    public static final String URL_GET_TASK = "http://www.mutualing.com/uservote/admin_edit/getTask";
    public static final String URL_GET_SHARED_TASK = "http://www.mutualing.com/uservote/admin_edit/getSharedTask";
    public static final String URL_LOGIN = "http://www.mutualing.com/uservote/admin_edit/login";
    public static final String URL_ASSIGN_MORE_USERS = "http://www.mutualing.com/uservote/admin_edit/assignMoreUsers";
    public static final String URL_INSERT_ANSWER = "http://www.mutualing.com/uservote/admin_edit/assignMoreUsers";
    public static final String URL_GET_USER_COMPLETE_DETAILS = "http://www.mutualing.com/uservote/admin_edit/getUserCompleteDetails";
    public static final String URL_ADD_FRIEND = "http://www.mutualing.com/uservote/admin_edit/addFriend";
    public static final String URL_SEARCH_USER = "http://www.mutualing.com/uservote/admin_edit/searchUser";
    public static final String URL_GET_FRIEND_LIST = "http://www.mutualing.com/uservote/admin_edit/getFriendList";
    public static final String URL_INSERT_PROFILE_PIC = "http://www.mutualing.com/insertUserPhoto";
    public static final String URL_GLOBAL_SEARCH ="http://www.mutualing.com/uservote/admin_edit/globalSearch";
    public static final String URL_FORGOT_PASSWORD ="http://www.mutualing.com/uservote/admin_edit/forgotPassword";
    public static final String URL_VERIFY_CHANGE_PASSWORD ="http://www.mutualing.com/uservote/admin_edit/verifyCodeAndChangePassword";
    public static final String URL_DEACTIVATE_ACCOUNT ="http://www.mutualing.com/uservote/admin_edit/deactiveAcc";
    public static final String URL_VERIFY_DEACTIVATE_ACCOUNT ="http://www.mutualing.com/uservote/admin_edit/verifyCodeAndDeactive";
    public ServiceQueryIntent() {
        super("ServiceQueryIntent");
    }
    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    Handler mHandler;
    @Override
    public void onCreate() {
        mHandler = new Handler(Looper.getMainLooper());
        super.onCreate();
    }
    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (QUERY_SET_DETAILS.equals(action)) {
                String emailId = intent.getStringExtra("EmailID");
                String phNumber = intent.getStringExtra("PhNumber");
                String password = intent.getStringExtra("Password");
                String firstName = intent.getStringExtra("FirstName");
                String lastName = intent.getStringExtra("LastName");
                handleActionQuerySetDetails(firstName, lastName, emailId, phNumber, password);
            }
            else if(QUERY_SET_PLACES.equals(action)){
                ArrayList<String> places = intent.getStringArrayListExtra("Places");
                handleActionQuerySetPlaces(places);
            }
            else if(QUERY_SET_EDUCATION.equals(action)){
                ArrayList<String> education = intent.getStringArrayListExtra("Education");
                handleActionQuerySetEducation(education);
            }
            else if(QUERY_SET_CAREER.equals(action)){
                ArrayList<String> career = intent.getStringArrayListExtra("Career");
                handleActionQuerySetCareer(career);
            }else if(QUERY_SET_HOBBIES.equals(action)){
                ArrayList<String> hobbies = intent.getStringArrayListExtra("Hobbies");
                handleActionQuerySetHobbies(hobbies);
            }else if(QUERY_INSERT_TASK.equals(action)){
                String taskContent = intent.getStringExtra("TaskContents");
                ArrayList<String> taskPeoples = intent.getStringArrayListExtra("TaskAssignees");
                handleActionQueryInsertTask(taskContent,taskPeoples);
            }else if(QUERY_DASHBOARD_DATA.equals(action)){
                handleActionQueryDashboardData();
            }else if(QUERY_GET_TASK.equals(action)){
                handleActionQueryGetTask();
            }else if(QUERY_GET_SHARED_TASK.equals(action)){
                handleActionQueryGetSharedTask();
            }else if(QUERY_LOGIN.equals(action)){
                String emailID = intent.getStringExtra("EmailID");
                String password = intent.getStringExtra("Password");
                handleActionQueryLogin(emailID, password);
            }else if(QUERY_ASSIGN_MORE_USERS.equals(action)){
                String taskID = intent.getStringExtra("TaskID");
                ArrayList<String> assignedPersons = intent.getStringArrayListExtra("AssignedPersons");
                handleActionQueryAssignMoreUsers(taskID,assignedPersons);
            }else if(QUERY_INSERT_ANSWER.equals(action)){
                String taskID = intent.getStringExtra("TaskID");
                String answer = intent.getStringExtra("Answer");
                handleActionQueryInsertAnswer(taskID, answer);
            }else if(QUERY_GET_USER_COMPLETE_DETAILS.equals(action)){
                String searchID = intent.getStringExtra("SearchID");
                handleActionQueryGetUserCompleteDetails(searchID);
            }else if(QUERY_ADD_FRIEND.equals(action)){
                int searchID = intent.getIntExtra("SearchID", -1);
                if(searchID != -1)
                    handleActionQueryAddFriend(searchID);
                else
                    Log.d("ServiceQueryIntent","Cannot add friend, invalid UserID");
            }else if(QUERY_SEARCH_USER.equals(action)){
                String search = intent.getStringExtra("SearchString");
                handleActionQuerySearchUser(search);
            }else if(QUERY_GET_FRIEND_LIST.equals(action)){
                handleActionQueryGetFriendList();
            }else if(QUERY_INSERT_PROFILE_PIC.equals(action)){
                handleActionQueryInsertProfilePic();
            }else if(QUERY_GLOBAL_SEARCH.equals(action)){
                String taskID = intent.getStringExtra("TaskID");
                String answer = intent.getStringExtra("Answer");
            }else if(QUERY_FORGOT_PASSWORD.equals(action)){
                String emailID = intent.getStringExtra("EmailID");

                handleActionQueryForgotPassword(emailID);
            }else if(QUERY_VERIFY_CHANGE_PASSWORD.equals(action)){
                //String USERid = intent.getStringExtra("UserID");
                String code = intent.getStringExtra("code");
                String password = intent.getStringExtra("password");
                handleActionQueryVerifyChangePassword(code,password);
            }
            else if(QUERY_VERIFY_DEACTIVATE_ACCOUNT.equals(action)){
                int USERid = intent.getIntExtra("UserID",0);
                String Emailid = intent.getStringExtra("EmailID");
                String code = intent.getStringExtra("code");
                handleActionQueryVerifyDeactivate(code,Emailid,USERid);
            }
        }
    }

    private void handleActionQueryInsertProfilePic()
    {
        try{
            Log.d("ServiceQueryIntent","Query Insert_profile_pic starting");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            Uri uri = Uri.parse(sharedPreferences.getString("ProfPicUri","NaN"));
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
            byte[] imageBytes = byteArrayOutputStream.toByteArray();
            String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            JSONObject jsonObject = new JSONObject().put("UserID", userID).put("UserPhoto", encodedImage);
            URL url = new URL(URL_INSERT_PROFILE_PIC);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            Log.d("ServiceQueryIntent", "Reading userid");
            while ((temporaryString = bufferedReader.readLine()) != null) {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent", "Read userid: " + inputString);

        }catch(MalformedURLException ex)
        {
            Log.d("ServiceQueryIntent", "MUE " + ex.getMessage());
        }catch(ProtocolException pe)
        {
            Log.d("ServiceQueryIntent", "PE " + pe.getMessage());
        }catch (JSONException je)
        {
            Log.d("ServiceQueryIntent", "JE " + je.getMessage());
        }catch (IOException ioe)
        {
            Log.d("ServiceQueryIntent", "IOE " + ioe.getMessage());
        }
    }

    private void handleActionQueryGetFriendList(){

        try{
            Log.d("ServiceQueryIntent","Query Get_Friend_List starting");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("UserID", userID);
            URL url = new URL(URL_GET_FRIEND_LIST);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_GET_FRIEND_LIST);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
            Intent intent = new Intent("PopulateTheListViewFriendList");
            intent.putExtra("JSONArrayString",inputString);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }

    }

    private void handleActionQueryForgotPassword(String emailID){
        try{
            Log.d("ServiceQueryIntent","Query forgot password");
            String temporaryString, inputString = "";
            JSONObject jsonObject = new JSONObject().put("EmailID", emailID);
            URL url = new URL(URL_FORGOT_PASSWORD);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_FORGOT_PASSWORD);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
            if(!(inputString.equals(""))) {
                mHandler.post(new MyRunnable(inputString));
                Intent broadcastIntent = new Intent("SentEmail");
                broadcastIntent.putExtra("Successfullysent", "true");
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            }
            else{
                mHandler.post(new MyRunnable("Communication Problem"));
                Intent broadcastIntent = new Intent("SentEmail");
                broadcastIntent.putExtra("Successfullysent", "false");
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            }
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }
    }

    private void handleActionQueryVerifyChangePassword(String code,String password){
        try{
            Log.d("ServiceQueryIntent","Query verify change password");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            String EmailID = sharedPreferences.getString("EmailID","String");
            JSONObject jsonObject = new JSONObject().put("code",code).put("password",password).put("EmailID",EmailID);
            URL url = new URL(URL_VERIFY_CHANGE_PASSWORD);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_VERIFY_CHANGE_PASSWORD);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
            if(inputString.equals("1")) {
                mHandler.post(new MyRunnable(inputString));
                Intent broadcastIntent = new Intent("ChangePassword");
                broadcastIntent.putExtra("Successfullychange", "true");
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            }
            else{
                mHandler.post(new MyRunnable("Communication Problem"));
                Intent broadcastIntent = new Intent("ChangePassword");
                broadcastIntent.putExtra("Successfullychange", "false");
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            }
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }
    }

    private void handleActionQueryVerifyDeactivate(String code,String EmailId,int userId){
        try{
            Log.d("ServiceQueryIntent","Query verify change password");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            String EmailID = sharedPreferences.getString("EmailID","String");
            JSONObject jsonObject = new JSONObject().put("EmailID",EmailID).put("UserID",userId).put("code",code);
            URL url = new URL(URL_VERIFY_DEACTIVATE_ACCOUNT);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_VERIFY_DEACTIVATE_ACCOUNT);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
            if(inputString.equals("1")) {
                mHandler.post(new MyRunnable(inputString));
                Intent broadcastIntent = new Intent("ChangePassword");
                broadcastIntent.putExtra("Successfullychange", "true");
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            }
            else{
                mHandler.post(new MyRunnable("Communication Problem"));
                Intent broadcastIntent = new Intent("ChangePassword");
                broadcastIntent.putExtra("Successfullychange", "false");
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            }
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }
    }
    private void handleActionQuerySearchUser(String search){
        try{
            Log.d("ServiceQueryIntent","Query Search User starting");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("UserID", userID).put("search", search);
            URL url = new URL(URL_SEARCH_USER);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_SEARCH_USER);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
            Intent intent = new Intent("PopulateTheListView");
            intent.putExtra("JSONArrayString",inputString);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }
    }

    private void handleActionQueryAddFriend(int searchID){
        try{
            Log.d("ServiceQueryIntent","Query Add Friend starting");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("UserID", userID).put("SearchID", searchID);
            URL url = new URL(URL_ADD_FRIEND);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_ADD_FRIEND);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            Log.d("ServiceQueryIntent","Got input stream");
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
            if(new JSONObject(inputString).getInt("var") == 1){
                mHandler.post(new MyRunnable("Successfully added friend"));
            }
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", "Error: "+ex.getMessage());
        }
    }

    private void handleActionQueryGetUserCompleteDetails(String searchID){
        try{
            Log.d("ServiceQueryIntent","Query Get User Complete Details starting");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("UserID", userID).put("SearchID", searchID);
            URL url = new URL(URL_GET_USER_COMPLETE_DETAILS);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_GET_USER_COMPLETE_DETAILS);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Intent intent = new Intent("GotDetails");
            intent.putExtra("JSONArrayString", inputString);
            LocalBroadcastManager.getInstance(this).sendBroadcast(intent);
            Log.d("ServiceQueryIntent","Read input: "+inputString);
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }
    }

    private void handleActionQueryInsertAnswer(String taskID, String answer){
        try{
            Log.d("ServiceQueryIntent","Query Insert Answer starting");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("UserID", userID).put("TaskID", taskID).put("Answer", answer);
            URL url = new URL(URL_INSERT_ANSWER);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_INSERT_ANSWER);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }
    }

    private void handleActionQueryAssignMoreUsers(String taskID, ArrayList<String> assignedPersons){
        try{
            Log.d("ServiceQueryIntent","Query Assign More Users starting");
            JSONArray jsonArray = new JSONArray();
            for(int i = 1; i < assignedPersons.size(); i++){
                jsonArray.put(i-1,assignedPersons.get(i));
            }
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("UserID", userID).put("TaskID", taskID).put("AssignedPersons", jsonArray);
            URL url = new URL(URL_ASSIGN_MORE_USERS);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_ASSIGN_MORE_USERS);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }
    }

    private void handleActionQueryLogin(String emailID, String password){
        try{
            Log.d("ServiceQueryIntent","Query Login starting");
            String temporaryString, inputString = "";
            JSONObject jsonObject = new JSONObject().put("EmailID", emailID).put("Pasword", password);
            URL url = new URL(URL_LOGIN);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_LOGIN);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }
    }

    private void handleActionQueryGetSharedTask(){
        try {
            Log.d("ServiceQueryIntent", "Query GET SHARED TASK starting");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID", 0);
            JSONObject jsonObject = new JSONObject().put("UserID", userID);
            URL url = new URL(URL_GET_SHARED_TASK);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_GET_SHARED_TASK);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
            JSONArray jsonInputArray = (new JSONObject(inputString)).getJSONArray("var");
            Log.d("ServiceQueryIntent", "JSONArray: "+jsonInputArray.toString());
            /*Intent broadcastToActivityDashboard = new Intent(ActivityDashboard.ASSIGNED_TASK_RECEIVED);
            broadcastToActivityDashboard.putExtra("DASHBOARD_DATA",jsonInputArray.toString());
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastToActivityDashboard);*/
        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }
    }

    private void handleActionQueryGetTask(){
        try {
            Log.d("ServiceQueryIntent", "Query GET TASK starting");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID", 0);
            JSONObject jsonObject = new JSONObject().put("UserID", userID);
            URL url = new URL(URL_GET_TASK);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_GET_TASK);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
            JSONArray jsonInputArray = (new JSONObject(inputString)).getJSONArray("var");
            Log.d("ServiceQueryIntent", "JSONArray: "+jsonInputArray.toString());
            /*Intent broadcastToActivityDashboard = new Intent(ActivityDashboard.ASSIGNED_TASK_RECEIVED);
            broadcastToActivityDashboard.putExtra("DASHBOARD_DATA",jsonInputArray.toString());
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastToActivityDashboard);*/
        }catch(Exception ex){
            Log.d("ServiceQueryIntent",ex.getMessage());
        }
    }

    private void handleActionQueryDashboardData(){
        try {
            Log.d("ServiceQueryIntent", "Query Main Data starting");
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID", 0);
            JSONObject jsonObject = new JSONObject().put("UserID", userID);
            URL url = new URL(URL_DASHBOARD_DATA);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_DASHBOARD_DATA);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
            JSONArray jsonInputArray = (new JSONObject(inputString)).getJSONArray("var");
            Log.d("ServiceQueryIntent", "JSONArray: "+jsonInputArray.toString());
        }catch(Exception ex){
            Log.d("ServiceQueryIntent",ex.getMessage());
        }
    }
    private void handleActionQueryInsertTask(String taskContent, ArrayList<String> taskPeoples){
        try{
            Log.d("ServiceQueryIntent","Query Insert Task starting");
            JSONArray jsonArray = new JSONArray();
            for(int i = 1; i < taskPeoples.size(); i++){
                jsonArray.put(i-1,taskPeoples.get(i));
            }
            String temporaryString, inputString = "";
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("UserID", userID).put("TaskDetails", taskContent).put("AssignedPersons", jsonArray);
            URL url = new URL(URL_INSERT_TASK);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_INSERT_TASK);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            Log.d("ServiceQueryIntent","Sent JSON object: "+jsonObject.toString());
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read input: "+inputString);
            JSONObject jsonSubmitStatus = new JSONObject(inputString);
            Intent broadcastIntent = new Intent("CheckSubmitStatus");
            broadcastIntent.putExtra("SubmitStatus", jsonSubmitStatus.getInt("var"));
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);

        }catch(Exception ex){
            Log.d("ServiceQueryIntent", ex.getMessage());
        }
    }
    private void handleActionQuerySetDetails(String firstName, String lastName, String emailId, String phNumber, String password) {
        try {
            Log.d("ServiceQueryIntent","Set Details Service starting");
            String temporaryString, inputString = "";
            JSONObject jsonObject = new JSONObject().put("Details", new JSONObject().put("firstName", firstName).put("lastName", lastName).put("EmailID", emailId).put("PhoneNo", phNumber).put("Password", password));
            URL url = new URL(URL_SIGNUP_DETAILS);
            Log.d("ServiceQueryIntent","Contacting Server: "+ URL_SIGNUP_DETAILS);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            Log.d("ServiceQueryIntent","Sending JSON object: "+jsonObject.toString());
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            Log.d("ServiceQueryIntent","Reading userid");
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            Log.d("ServiceQueryIntent","Read userid: "+inputString);
            if(!(inputString.equals(""))) {
                JSONObject returnObject = new JSONObject(inputString);
                mHandler.post(new MyRunnable(inputString));
                SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("UserID",returnObject.getInt("var"));
                editor.apply();
                Intent broadcastIntent = new Intent("Signup1");
                broadcastIntent.putExtra("SuccessfulSignup1", "true");
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            }
            else{
                mHandler.post(new MyRunnable("Communication Problem"));
                Intent broadcastIntent = new Intent("Signup1");
                broadcastIntent.putExtra("SuccessfulSignup1", "false");
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            }
        }catch(JSONException jsonException)
        {
            Log.d("ServiceQueryIntentEx1",jsonException.getMessage());
            Intent broadcastIntent = new Intent("Signup1");
            broadcastIntent.putExtra("SuccessfulSignup1", "false");
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        }catch(MalformedURLException mue)
        {
            Log.d("ServiceQueryIntentEx2",mue.getMessage());
            Intent broadcastIntent = new Intent("Signup1");
            broadcastIntent.putExtra("SuccessfulSignup1", "false");
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        }catch(IOException ioe)
        {
            Log.d("ServiceQueryIntentEx3",ioe.getMessage());
            Intent broadcastIntent = new Intent("Signup1");
            broadcastIntent.putExtra("SuccessfulSignup1", "false");
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
        }

    }
    private void handleActionQuerySetPlaces(ArrayList<String> places){
        try{
            String temporaryString, inputString = "";
            JSONArray jsonArray = new JSONArray();
            for(int i = 1; i < places.size(); i++){
                jsonArray.put(i-1,places.get(i));
            }
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("Places",new JSONObject().put("Home",places.get(0)).put("Visited",jsonArray).put("UserID",userID));
            mHandler.post(new MyRunnable(jsonObject.toString()));
            URL url = new URL(URL_SIGNUP_PLACES);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            mHandler.post(new MyRunnable("Sent JSON"));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            Log.d("ServiceQueryIntent","Reading userid");
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            if(!inputString.equals(""))
                mHandler.post(new MyRunnable("Status: "+inputString));
            else
                mHandler.post(new MyRunnable("Didn't receive status"));
        }catch(JSONException je){
            Log.d("ServiceQueryIntent",je.getMessage());
        }catch(MalformedURLException mue){
            Log.d("ServiceQueryIntent",mue.getMessage());
        }catch(IOException io)
        {
            Log.d("ServiceQueryIntent",io.getMessage());
        }
    }
    private void handleActionQuerySetEducation(ArrayList<String> education){
        try{
            String temporaryString, inputString = "";
            JSONArray jsonArray = new JSONArray();
            for(int i = 0, j = 0; i < education.size(); i++){
                jsonArray.put(j++,new JSONObject().put("Field",education.get(i++)).put("Institution",education.get(i)));
            }
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("Education",jsonArray).put("UserID",userID);
            mHandler.post(new MyRunnable(jsonObject.toString()));
            URL url = new URL(URL_SIGNUP_EDUCATION);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            mHandler.post(new MyRunnable("Sent JSON"));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            Log.d("ServiceQueryIntent","Reading userid");
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            if(!inputString.equals(""))
                mHandler.post(new MyRunnable("Status: "+inputString));
            else
                mHandler.post(new MyRunnable("Didn't receive status"));
        }catch(Exception exception){
            Log.d("ServiceQueryIntent",exception.getMessage());
        }
    }
    private void handleActionQuerySetCareer(ArrayList<String> career){
        try{
            String temporaryString, inputString = "";
            JSONArray jsonArray = new JSONArray();
            for(int i = 0, j = 0; i < career.size(); i++){
                jsonArray.put(j++,new JSONObject().put("Name",career.get(i++)).put("Designation",career.get(i++)).put("Domain",career.get(i)));
            }
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("Career",jsonArray).put("UserID",userID);
            mHandler.post(new MyRunnable(jsonObject.toString()));
            URL url = new URL(URL_SIGNUP_CAREER);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            mHandler.post(new MyRunnable("Sent JSON"));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            Log.d("ServiceQueryIntent","Reading userid");
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            if(!inputString.equals(""))
                mHandler.post(new MyRunnable("Status: "+inputString));
            else
                mHandler.post(new MyRunnable("Didn't receive status"));
        }catch(Exception exception){
            Log.d("ServiceQueryIntent",exception.getMessage());
        }
    }
    private void handleActionQuerySetHobbies(ArrayList<String> hobbies){
        try{
            String temporaryString, inputString = "";
            JSONArray jsonArray = new JSONArray();
            for(int i = 0; i < hobbies.size(); i++){
                jsonArray.put(i,hobbies.get(i));
            }
            SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),Context.MODE_PRIVATE);
            int userID = sharedPreferences.getInt("UserID",0);
            JSONObject jsonObject = new JSONObject().put("Hobbies",jsonArray).put("UserID",userID);
            mHandler.post(new MyRunnable(jsonObject.toString()));
            URL url = new URL(URL_SIGNUP_HOBBIES);
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setRequestMethod("POST");
            urlConnection.setDoInput(true);
            urlConnection.setDoOutput(true);
            urlConnection.connect();
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(urlConnection.getOutputStream());
            outputStreamWriter.write(jsonObject.toString());
            outputStreamWriter.flush();
            mHandler.post(new MyRunnable("Sent JSON"));
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            Log.d("ServiceQueryIntent","Reading userid");
            while((temporaryString = bufferedReader.readLine())!=null)
            {
                inputString = inputString + temporaryString;
            }
            if(!inputString.equals("")) {
                mHandler.post(new MyRunnable("Status: " + inputString));
                Intent startMainActivityIntent = new Intent(this, ActivityDashboard.class);
                startMainActivityIntent.setAction(Intent.ACTION_VIEW);
                startMainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startMainActivityIntent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(startMainActivityIntent);
                sendBroadcast(new Intent("closeActivity"));
            }
            else
                mHandler.post(new MyRunnable("Didn't receive status"));
        }catch(Exception exception){
            Log.d("ServiceQueryIntent",exception.getMessage());
        }
    }

    public class MyRunnable implements Runnable{
        private String data;
        public MyRunnable(String data){
            this.data = data;
        }
        @Override
        public void run(){
            Toast.makeText(getApplicationContext(), "User ID: "+data, Toast.LENGTH_LONG).show();
        }
    }
}
