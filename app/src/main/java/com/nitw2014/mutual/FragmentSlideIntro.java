package com.nitw2014.mutual;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

/**
 * Created by prabhakar on 25/03/17.
 */

public class FragmentSlideIntro extends Fragment {

    private int mPage;
    FragmentActivity context;

    @Override
    public void onAttach(Context context) {
        this.context = (FragmentActivity) context;
        super.onAttach(context);
    }

    public static FragmentSlideIntro newInstance(int page)
    {
        FragmentSlideIntro fragment = new FragmentSlideIntro();
        Bundle b = new Bundle();
        b.putInt("page", page);
        fragment.setArguments(b);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(!getArguments().containsKey("page"))
            throw new RuntimeException("Fragment must contain a \"page\" argument!");
        mPage = getArguments().getInt("page");
        Log.d("ActivitySlideIntro","mPage value= "+mPage);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layoutResId = R.layout.fragment_intro_page0;

        switch(mPage)
        {
            case 0:
                layoutResId = R.layout.fragment_intro_page0;
                break;
            case 1:
                layoutResId = R.layout.fragment_intro_page1;
                break;
            case 2:
                layoutResId = R.layout.fragment_intro_page2;
        }

        context.getWindow().setFlags(WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED);
        View view = getActivity().getLayoutInflater().inflate(layoutResId, container, false);
        view.isHardwareAccelerated();
        Log.d("HardwareAcceleration","" + view.isHardwareAccelerated());
        view.setTag(mPage);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView finishIntro = (TextView) view.findViewById(R.id.intro_finish);
        TextView skipIntro2 = (TextView) view.findViewById(R.id.skip_intro2);
        TextView skipIntro1 = (TextView) view.findViewById(R.id.skip_intro1);
        try {
            finishIntro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent signupActivityIntent = new Intent(context, ActivityLogin.class);
                    SharedPreferences sharedPreferences = context.getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("IntroDone", 1);
                    editor.apply();
                    startActivity(signupActivityIntent);
                    context.finish();
                }
            });
        }catch(NullPointerException nue)
        {
            Log.d("FragmentSlideIntro", "NullPointerException");
        }
        try{
            skipIntro2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent signupActivityIntent = new Intent(context, ActivityLogin.class);
                    SharedPreferences sharedPreferences = context.getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("IntroDone", 1);
                    editor.apply();
                    startActivity(signupActivityIntent);
                    context.finish();
                }
            });
        }catch(NullPointerException nue)
        {
            Log.d("FragmentSlideIntro", "NullPointerException");
        }
        try
        {
            skipIntro1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent signupActivityIntent = new Intent(context.getApplicationContext(), ActivityLogin.class);
                    SharedPreferences sharedPreferences = context.getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putInt("IntroDone", 1);
                    editor.apply();
                    startActivity(signupActivityIntent);
                    context.finish();
                }
            });
        }catch(NullPointerException nue)
        {
            Log.d("FragmentSlideIntro", "NullPointerException");
        }
    }
}
