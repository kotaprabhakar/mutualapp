package com.nitw2014.mutual;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.SearchManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.database.ContentObserver;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.ContactsContract;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.Loader;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.LayoutInflaterCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static com.nitw2014.mutual.ServiceQueryIntent.QUERY_INSERT_PROFILE_PIC;

public class ActivityMutualCenter extends AppCompatActivity {

    private static final Uri URI_TO_OBSERVE = ProviderContractTask.FriendsList.CONTENT_URI;
    private static final int REQ_CODE_PICK_IMAGE = 100;
    public static final String MUTUAL_IMAGES_DIR = "mutualingimages";
    public static final String PROFILE_PIC = "profilepic_mutualing";
    private static final int REQUEST_WRITE_EXTERNAL_STORAGE = 0;
    public static Uri pictureUri = null;

    RelativeLayout statusBarLayout;
    RelativeLayout searchRecyclerViewLayout;
    RecyclerView recyclerViewSearch;
    SharedPreferences sharedPreferences;
    MutualCenterObserver mutualCenterObserver;
    RecyclerView manageYourCircle;
    File dirMutualingImages;
    File dpMutualing;
    AdapterMutualCenterRecycler adapter;
    AdapterSearchRecyclerView adapterSearchRecyclerView;
   // ImageView profPic;
    LinearLayout mutualcenterLinearLayout;
    List<FeedMutualCenterRecycler> feedMutualCenterRecyclerList;
    List<FeedSearchRecyclerView> feedSearchRecyclerViewList;
    TextView moreFriendsView;
    TextView rv_empty_text;
    Context activityContext;
    Toolbar toolbar;
    BroadcastReceiver searchBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                String jsonArrayString = intent.getStringExtra("JSONArrayString");
                JSONArray jsonArray = new JSONArray(jsonArrayString);
                feedSearchRecyclerViewList.clear();
                for(int i = 0; i < jsonArray.length(); i++)
                {
                    FeedSearchRecyclerView feedSearchRecyclerView = new FeedSearchRecyclerView();
                    JSONObject jsonObject = (JSONObject) jsonArray.get(i);
                    feedSearchRecyclerView.setUserId(jsonObject.getString("id"));
                    feedSearchRecyclerView.setFriendName(jsonObject.getString("first_name") + " " + jsonObject.getString("last_name"));
                    feedSearchRecyclerViewList.add(feedSearchRecyclerView);
                }
                Log.d("ActivityMutualCenter", "Size of search query result: " + feedSearchRecyclerViewList.size());
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Log.d("ActivityMutualCenter", "Running on Ui Thread");
                        adapterSearchRecyclerView.notifyDataSetChanged();
                    }
                });
                Log.d("ActivityMutualCenter", "Received list of friends: " + jsonArrayString + " Length = " + jsonArrayString.length());
            }catch(Exception ex)
            {
                Log.d("ActivityMutualCenter","Exception in receiving search query: " + ex.getMessage());
            }
        }
    };

    //------------------------------LOADERCALLBACKS INTERFACE START---------------------------------



    //------------------------------LOADERCALLBACKS INTERFACE END-----------------------------------


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mutual_center);

        activityContext = this;

        sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),MODE_PRIVATE);

        recyclerViewSearch = (RecyclerView) findViewById(R.id.search_recyclerview_view);
        LinearLayoutManager searchLinearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewSearch.setLayoutManager(searchLinearLayoutManager);
        searchRecyclerViewLayout = (RelativeLayout) findViewById(R.id.search_recyclerview_mutualcenter);
        mutualcenterLinearLayout = (LinearLayout) findViewById(R.id.mut_center_linear_layout);

        feedSearchRecyclerViewList = new ArrayList<>();
        adapterSearchRecyclerView = new AdapterSearchRecyclerView(this, feedSearchRecyclerViewList);
        recyclerViewSearch.setAdapter(adapterSearchRecyclerView);

        manageYourCircle = (RecyclerView) findViewById(R.id.rv_manage_your_circle);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        manageYourCircle.setLayoutManager(linearLayoutManager);
        feedMutualCenterRecyclerList = new ArrayList<>();
        adapter = new AdapterMutualCenterRecycler(this, feedMutualCenterRecyclerList);
        manageYourCircle.setAdapter(adapter);
        HandlerThread observerThread = new HandlerThread("MutualCenterObserverThread");
        observerThread.start();
        mutualCenterObserver = new MutualCenterObserver(new Handler(observerThread.getLooper()));

        new Handler(observerThread.getLooper()).post(new Runnable() {
            @Override
            public void run() {
                retrieveFriendsFromContentProvider();
                new Handler(getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                    }
                });
            }
        });

        moreFriendsView = (TextView) findViewById(R.id.more_friends_view);
        rv_empty_text = (TextView) findViewById(R.id.rv_manage_your_circle_empty);
        if(adapter.getItemCount() == 0) {
            rv_empty_text.setVisibility(View.VISIBLE);
            manageYourCircle.setVisibility(View.GONE);
        }else{
            rv_empty_text.setVisibility(View.GONE);
            manageYourCircle.setVisibility(View.VISIBLE);
        }

        //ContextWrapper cw = new ContextWrapper(getApplicationContext());
        dirMutualingImages = getFilesDir();
        dpMutualing = new File(dirMutualingImages, PROFILE_PIC+".png");
        pictureUri = Uri.fromFile(dpMutualing);

//        profPic = (ImageView) findViewById(R.id.user_image_mutualcenter);
//        profPic.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
//                photoPickerIntent.setType("image/*");
//                startActivityForResult(photoPickerIntent, REQ_CODE_PICK_IMAGE);
//            }
//        });
//        if(!sharedPreferences.getString("ProfPicUri","NaN").equals("NaN"))
//            Glide.with(this).load(new File(Uri.parse(sharedPreferences.getString("ProfPicUri","NaN")).getPath())).into(profPic);

        statusBarLayout = (RelativeLayout) findViewById(R.id.below_statusbar_mutualcenter);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) statusBarLayout.getLayoutParams();
        lp.height = getStatusBarHeight();
        Log.d("ActivityMutualCenter", "StatusBar height= " + getStatusBarHeight());

        toolbar = (Toolbar)findViewById(R.id.toolbar_mutualcenter);
        toolbar.setTitle("Mutual Center");
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back_black);

      //  ((TextView)findViewById(R.id.user_name_mutualcenter)).setText("Hi " + sharedPreferences.getString("FirstName", "NaN") + " " + sharedPreferences.getString("LastName", "NaN"));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode)
        {
            case REQ_CODE_PICK_IMAGE:
                if (resultCode == RESULT_OK) {
                    Log.d("ActivityMutualCenter","Received Image");
                    try {
                        InputStream is = getContentResolver().openInputStream(data.getData());
                        Bitmap bitmap = BitmapFactory.decodeStream(is);
                        new LoadProfilePic().execute(bitmap);
                        Log.d("ActivityMutualCenter", "Started service");
                    }catch (FileNotFoundException fnfe)
                    {
                        Log.d("ActivityMutualCenter", "fnfe: " + fnfe.getMessage());
                    }

                }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getContentResolver().registerContentObserver(URI_TO_OBSERVE, true, mutualCenterObserver);
        LocalBroadcastManager.getInstance(this).registerReceiver(searchBroadcastReceiver, new IntentFilter("PopulateTheListView"));
    }

    @Override
    protected void onPause() {
        super.onPause();
        getContentResolver().unregisterContentObserver(mutualCenterObserver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(searchBroadcastReceiver);
    }

    void retrieveFriendsFromContentProvider()
    {
        int size = 0;
        Cursor cursor = getContentResolver().query(URI_TO_OBSERVE, new String[]{ProviderContractTask.FriendsList.FRIEND}, null, null, ProviderContractTask.FriendsList.DEFAULT_SORT_ORDER);
        feedMutualCenterRecyclerList.clear();
        if(cursor!=null)
        {
            while (cursor.moveToNext())
            {
                FeedMutualCenterRecycler feedMutualCenterRecycler = new FeedMutualCenterRecycler();
                feedMutualCenterRecycler.setFriendTextMutualCenter(cursor.getString(0));
                Log.d("ActivityMutualCenter", "Query from Friends table: " + cursor.getString(0));
                feedMutualCenterRecyclerList.add(feedMutualCenterRecycler);
                size++;
                if(size > 8)
                {
                    moreFriendsView.setVisibility(View.VISIBLE);
                    moreFriendsView.setClickable(true);
                    moreFriendsView.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                        }
                    });
                }
            }
        }
        cursor.close();

        if(adapter.getItemCount() == 0) {
            rv_empty_text.setVisibility(View.VISIBLE);
            manageYourCircle.setVisibility(View.GONE);
        }else{
            rv_empty_text.setVisibility(View.GONE);
            manageYourCircle.setVisibility(View.VISIBLE);
        }

    }

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_mutual_center, menu);
        configureSearchView(menu);
        return true;
    }

    void configureSearchView(Menu menu)
    {

        final MenuItem searchItem = menu.findItem(R.id.search_menu);
        SearchView searchView = (SearchView) searchItem.getActionView();

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Intent intent = new Intent(activityContext, ServiceQueryIntent.class);
                intent.setAction(ServiceQueryIntent.QUERY_SEARCH_USER);
                intent.putExtra("SearchString", newText);
                startService(intent);
                return true;
            }
        });
        getSupportActionBar().setHomeAsUpIndicator(getDrawable(R.drawable.ic_action_arrow_back));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                // Called when SearchView is collapsing
                if (searchItem.isActionViewExpanded()) {
                    //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back);
                    Log.d("ActivityMutualCenter", "SearchView collapsed");
                    mutualcenterLinearLayout.setVisibility(View.VISIBLE);
                    searchRecyclerViewLayout.setVisibility(View.GONE);
                    recyclerViewSearch.setVisibility(View.GONE);
                    animateSearchToolbar(1, false, false);
                }
                return true;
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                // Called when SearchView is expanding
                //getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_action_arrow_back_black);
                animateSearchToolbar(1, true, true);
                Log.d("ActivityMutualCenter", "SearchView expanded");
                searchRecyclerViewLayout.setVisibility(View.VISIBLE);
                recyclerViewSearch.setVisibility(View.VISIBLE);
                mutualcenterLinearLayout.setVisibility(View.GONE);
                return true;
            }
        });
    }

    public void animateSearchToolbar(int numberOfMenuIcon, boolean containsOverflow, boolean show) {

        toolbar.setBackgroundColor(ContextCompat.getColor(this, android.R.color.white));
        //toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary));
        //mDrawerLayout.setStatusBarBackgroundColor(ContextCompat.getColor(this, R.color.quantum_grey_600));

        if (show) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int width = toolbar.getWidth() -
                        (containsOverflow ? getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material) : 0) -
                        ((getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) * numberOfMenuIcon) / 2);
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(toolbar,
                        isRtl(getResources()) ? toolbar.getWidth() - width : width, toolbar.getHeight() / 2, 0.0f, (float) width);
                createCircularReveal.setDuration(250);
                createCircularReveal.start();
            } else {
                TranslateAnimation translateAnimation = new TranslateAnimation(0.0f, 0.0f, (float) (-toolbar.getHeight()), 0.0f);
                translateAnimation.setDuration(220);
                toolbar.clearAnimation();
                toolbar.startAnimation(translateAnimation);
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                int width = toolbar.getWidth() -
                        (containsOverflow ? getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_overflow_material) : 0) -
                        ((getResources().getDimensionPixelSize(R.dimen.abc_action_button_min_width_material) * numberOfMenuIcon) / 2);
                Animator createCircularReveal = ViewAnimationUtils.createCircularReveal(toolbar,
                        isRtl(getResources()) ? toolbar.getWidth() - width : width, toolbar.getHeight() / 2, (float) width, 0.0f);
                createCircularReveal.setDuration(250);
                createCircularReveal.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        toolbar.setBackgroundColor(getThemeColor(activityContext, R.attr.colorPrimary));
                        //mDrawerLayout.setStatusBarBackgroundColor(getThemeColor(MainActivity.this, R.attr.colorPrimaryDark));
                    }
                });
                createCircularReveal.start();
            } else {
                AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
                Animation translateAnimation = new TranslateAnimation(0.0f, 0.0f, 0.0f, (float) (-toolbar.getHeight()));
                AnimationSet animationSet = new AnimationSet(true);
                animationSet.addAnimation(alphaAnimation);
                animationSet.addAnimation(translateAnimation);
                animationSet.setDuration(220);
                animationSet.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        toolbar.setBackgroundColor(getThemeColor(activityContext, R.attr.colorPrimary));
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                toolbar.startAnimation(animationSet);
            }
            //mDrawerLayout.setStatusBarBackgroundColor(ThemeUtils.getThemeColor(MainActivity.this, R.attr.colorPrimaryDark));
        }
    }

    private boolean isRtl(Resources resources) {
        return resources.getConfiguration().getLayoutDirection() == View.LAYOUT_DIRECTION_RTL;
    }

    private static int getThemeColor(Context context, int id) {
        Resources.Theme theme = context.getTheme();
        TypedArray a = theme.obtainStyledAttributes(new int[]{id});
        int result = a.getColor(0, 0);
        a.recycle();
        return result;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId())
        {
            case android.R.id.home:
                this.finish();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    //======================================INNER CLASSES===========================================

    class FeedSearchRecyclerView {

        private String userId;
        private  String friendName;
        private String imageFileName;

        public String getUserId() {
            return userId;
        }

        public String getFriendName() {
            return friendName;
        }

        public String getImageFileName() {
            return imageFileName;
        }

        public void setFriendName(String friendName) {
            this.friendName = friendName;
        }

        public void setImageFileName(String imageFileName) {
            this.imageFileName = imageFileName;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }
    }

    class AdapterSearchRecyclerView extends RecyclerView.Adapter<AdapterSearchRecyclerView.SearchRecyclerViewHolder>{

        private List<FeedSearchRecyclerView> feedSearchRecyclerViewList;
        Context applicationContext;

        AdapterSearchRecyclerView(Context context, List<FeedSearchRecyclerView> feedSearchRecyclerViewList)
        {
            this.feedSearchRecyclerViewList = feedSearchRecyclerViewList;
            this.applicationContext = context;
        }

        @Override
        public SearchRecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_search, parent, false);
            return new SearchRecyclerViewHolder(view);
        }

        @Override
        public int getItemCount() {
            return (feedSearchRecyclerViewList!= null)?feedSearchRecyclerViewList.size():0;
        }

        @Override
        public void onBindViewHolder(final SearchRecyclerViewHolder holder, final int position) {
            final FeedSearchRecyclerView feedSearchRecyclerView = feedSearchRecyclerViewList.get(position);
            holder.getItemView().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("ActivityMutualCenter", "Clicked on search result # " + position);
                    SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences), Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("SearchID",feedSearchRecyclerView.getUserId());
                    editor.apply();
                    //Intent intent = new Intent(activityContext, ServiceQueryIntent.class);
                    Intent intentProfile = new Intent(activityContext, ActivityFriendProfile.class);
                    //intent.setAction(ServiceQueryIntent.QUERY_GET_USER_COMPLETE_DETAILS);
                    //intent.putExtra("SearchID", feedSearchRecyclerView.getUserId());
                    intentProfile.putExtra("UserName", feedSearchRecyclerView.getFriendName());
                    intentProfile.putExtra("SearchID", feedSearchRecyclerView.getUserId());
                    //startService(intent);
                    startActivity(intentProfile);
                }
            });
            Log.d("ActivityMutualCenter", "Binding View- "+feedSearchRecyclerView.getFriendName());
            holder.bindModel(feedSearchRecyclerView.getFriendName());
        }

        class SearchRecyclerViewHolder extends RecyclerView.ViewHolder{

            ImageView imageView;
            TextView textView;
            View itemView;

            SearchRecyclerViewHolder(View itemView){
                super(itemView);
                this.itemView = itemView;
                imageView = (ImageView) itemView.findViewById(R.id.friend_label_search_view);
                textView = (TextView) itemView.findViewById(R.id.card_view_search_id);
            }

            View getItemView() {
                return itemView;
            }

            void bindModel(String textSearch)
            {
                textView.setText(textSearch);
            }

        }
    }

    //----------------------------------------------------------------------------------------------

    class FeedMutualCenterRecycler {

        String friendTextMutualCenter;

        void setFriendTextMutualCenter(String friendTextMutualCenter) {
            this.friendTextMutualCenter = friendTextMutualCenter;
        }

        String getFriendTextMutualCenter() {
            return friendTextMutualCenter;
        }
    }

    class AdapterMutualCenterRecycler extends RecyclerView.Adapter<AdapterMutualCenterRecycler.MutualCenterViewHolder> {

        private List<FeedMutualCenterRecycler> feedMutualCenterRecyclerList;
        private Context applicationContext;

        AdapterMutualCenterRecycler(Context context, List<FeedMutualCenterRecycler> feedMutualCenterRecyclerList)
        {
            this.feedMutualCenterRecyclerList = feedMutualCenterRecyclerList;
            this.applicationContext = context;
        }

        @Override
        public int getItemCount() {
            return (null != feedMutualCenterRecyclerList ? feedMutualCenterRecyclerList.size():0);
        }

        @Override
        public void onBindViewHolder(MutualCenterViewHolder holder, int position) {
            FeedMutualCenterRecycler feedMutualCenterRecycler = feedMutualCenterRecyclerList.get(position);
            holder.bindModel(feedMutualCenterRecycler.getFriendTextMutualCenter());
        }

        @Override
        public MutualCenterViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.column_rv_mutualcenter, parent, false);
            return new MutualCenterViewHolder(view);
        }

        class MutualCenterViewHolder extends RecyclerView.ViewHolder
        {
            ImageView friendImageView;
            TextView friendTextView;

            MutualCenterViewHolder(View itemView)
            {
                super(itemView);
                friendImageView = (ImageView) itemView.findViewById(R.id.rv_column_image);
                friendTextView = (TextView) itemView.findViewById(R.id.rv_column_text);
            }

            void bindModel(String textLabelMutualCenter)
            {
                friendTextView.setText(textLabelMutualCenter);
            }

        }

    }

    //----------------------------------------------------------------------------------------------

    private class LoadProfilePic extends AsyncTask<Bitmap, Void, Void>
    {

        Bitmap bitmap;
        @Override
        protected Void doInBackground(Bitmap... data) {
            try {
                bitmap = data[0];
                Log.d("ActivityMutualCenter", "Received Bitmap");
                FileOutputStream fos = new FileOutputStream(dpMutualing);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, fos);
                Log.d("ActivityMutualCenter", "Compressed Image to file");
            }catch (FileNotFoundException ex)
            {
                Log.d("ActivityMutualCenter", "FNFE: " + ex.getMessage());
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            Log.d("ActivityMutualCenter", "Going to set image as profile pic");
//            Glide.with(activityContext).load(new File(pictureUri.getPath())).into(profPic);
//            (sharedPreferences.edit()).putString("ProfPicUri", pictureUri.toString()).apply();
//            Log.d("pictureUri",pictureUri.toString());
//            Intent serviceIntent = new Intent(activityContext, ServiceQueryIntent.class);
//            serviceIntent.setAction(QUERY_INSERT_PROFILE_PIC);
//            startService(serviceIntent);
//         //   profPic.setImageBitmap(bitmap);
//            Log.d("ActivityMutualCenter","Set image as profile pic");
//            Log.d("ActivityMutualCenter",QUERY_INSERT_PROFILE_PIC);
        }
    }


    private class MutualCenterObserver extends ContentObserver{

        MutualCenterObserver(Handler handler)
        {
            super(handler);
        }

        @Override
        public boolean deliverSelfNotifications() {
            return true;
        }

        @Override
        public void onChange(boolean selfChange) {
            super.onChange(selfChange);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {
            super.onChange(selfChange, uri);
            retrieveFriendsFromContentProvider();
            new Handler(getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    adapter.notifyDataSetChanged();
                }
            });
        }
    }

}
