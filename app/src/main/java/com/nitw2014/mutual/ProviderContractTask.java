package com.nitw2014.mutual;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by prabhakar on 25/09/16.
 */

final class ProviderContractTask {

    static final String AUTHORITY = "com.nitw2014.mutual.taskprovider";

    private ProviderContractTask(){}

    static final class FriendsList implements BaseColumns{

        private FriendsList(){}

        static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/friends");

        static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.mutualing.friends";

        static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.mutualing.friends";

        static final String DEFAULT_SORT_ORDER = "friend DESC";

        /**
         * TASK description
         * <p>Type: TEXT</p>
         */
        static final String FRIEND = "friend";

        /**
         * TASK_ID
         * <p>Type: TEXT</p>
         */
        static final String FRIEND_ID = "friend_id";

    }

    static final class SharedHistory implements BaseColumns{

        private SharedHistory(){}

        //The content:// style URL for this table
        static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/shared");

        //The MIME type of a {@link CONTENT_URI} providing a directory of tasks
        static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.mutualing.shared";

        //The MIME type of a {@link CONTENT_URI} providing a sub-directory of a single task
        static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.mutualing.shared";

        //The default sort order for this table
        static final String DEFAULT_SORT_ORDER = "task DESC";

        //Columns:

        /**
         * TASK description
         * <p>Type: TEXT</p>
         */
        static final String TASK = "task";

        /**
         * TASK_ID
         * <p>Type: TEXT</p>
         */
        static final String TASK_ID = "task_id";

        /**
         * SUBMITTER name
         * <p>Type: TEXT</p>
         */
        static final String SUBMITTER = "submitter";

        /**
         * SUBMITTER's User ID
         * <p>Type: TEXT</p>
         */
        static final String SUBMITTER_ID = "submitter_id";

    }

    static final class PerformedHistory implements BaseColumns{

        private PerformedHistory(){}

        //The content:// style URL for this table
        static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/performed");

        //The MIME type of a {@link CONTENT_URI} providing a directory of tasks
        static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.mutualing.performed";

        //The MIME type of a {@link CONTENT_URI} providing a sub-directory of a single task
        static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.mutualing.performed";

        //The default sort order for this table
        static final String DEFAULT_SORT_ORDER = "task DESC";

        //Columns:

        /**
         * TASK description
         * <p>Type: TEXT</p>
         */
        static final String TASK = "task";

        /**
         * TASK_ID
         * <p>Type: TEXT</p>
         */
        static final String TASK_ID = "task_id";

        /**
         * ANSWER description
         */
        static final String ANSWER = "answer";

        /**
         * SUBMITTER name
         * <p>Type: TEXT</p>
         */
        static final String SUBMITTER = "submitter";

        /**
         * SUBMITTER's User ID
         * <p>Type: TEXT</p>
         */
        static final String SUBMITTER_ID = "submitter_id";

    }

    static final class AskedHistory implements BaseColumns{

        private AskedHistory(){}

        //The content:// style URL for this table
        static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/asked");

        //The MIME type of a {@link CONTENT_URI} providing a directory of tasks
        static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.mutualing.asked";

        //The MIME type of a {@link CONTENT_URI} providing a sub-directory of a single task
        static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.mutualing.asked";

        //The default sort order for this table
        static final String DEFAULT_SORT_ORDER = "task DESC";

        //Columns:

        /**
         * TASK description
         * <p>Type: TEXT</p>
         */
        static final String TASK = "task";

        /**
         * TASK_ID
         * <p>Type: TEXT</p>
         */
        static final String TASK_ID = "task_id";

        /**
         * TASK_ANSWER
         * <p>Type: TEXT</p>
         */
        static final String TASK_ANSWER = "task_answer";

        /**
         * SUBMITTER name
         * <p>Type: TEXT</p>
         */
        static final String SUBMITTER = "submitter";

        /**
         * SUBMITTER's User ID
         * <p>Type: TEXT</p>
         */
        static final String SUBMITTER_ID = "submitter_id";

    }

    static final class TasksAssigned implements BaseColumns{

        private TasksAssigned(){}

        //The content:// style URL for this table
            static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY + "/tasks");

        //The MIME type of a {@link CONTENT_URI} providing a directory of tasks
            static final String CONTENT_TYPE = "vnd.android.cursor.dir/vnd.mutualing.task";

        //The MIME type of a {@link CONTENT_URI} providing a sub-directory of a single task
            static final String CONTENT_ITEM_TYPE = "vnd.android.cursor.item/vnd.mutualing.task";

        //The default sort order for this table
            static final String DEFAULT_SORT_ORDER = "task DESC";

        //Columns:

            /**
             * TASK description
             * <p>Type: TEXT</p>
             */
                static final String TASK = "task";

            /**
             * TASK_ID
             * <p>Type: TEXT</p>
             */
                static final String TASK_ID = "task_id";

            /**
             * SUBMITTER name
             * <p>Type: TEXT</p>
             */
                static final String SUBMITTER = "submitter";

            /**
             * SUBMITTER's User ID
             * <p>Type: TEXT</p>
             */
                static final String SUBMITTER_ID = "submitter_id";
    }

}
