package com.nitw2014.mutual;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by prabhakar on 18/09/16.
 */
public class AdapterSignupDetails extends FragmentPagerAdapter implements Serializable{

    Context context;
    public ViewPager detailsViewPager;
    int viewcount = 4;

    public AdapterSignupDetails(FragmentManager manager,Context context, ViewPager detailsViewPager){
        super(manager);
        this.context = context;
        this.detailsViewPager = detailsViewPager;
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment;

        switch(position){
            case 0:
                Bundle placeDetailsBundle = new Bundle();
                placeDetailsBundle.putSerializable("AdapterObject", this);
                fragment = new FragmentSignupPlace();
                fragment.setArguments(placeDetailsBundle); break;
            case 1:
                Bundle careerDetailsBundle = new Bundle();
                careerDetailsBundle.putSerializable("AdapterObject", this);
                fragment = new FragmentSignupCareer();
                fragment.setArguments(careerDetailsBundle); break;
            case 2:
                Bundle educationDetailsBundle = new Bundle();
                educationDetailsBundle.putSerializable("AdapterObject", this);
                fragment = new FragmentSignupEducation();
                fragment.setArguments(educationDetailsBundle);break;
            case 3:
                fragment = new FragmentSignupHobbies();break;
            default: fragment = null;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return viewcount;
    }
}
