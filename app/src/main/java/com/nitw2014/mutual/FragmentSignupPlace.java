package com.nitw2014.mutual;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Sai Karthik on 6/12/2016.
 */
public class FragmentSignupPlace extends Fragment {
    private FragmentActivity placeFragmentContext;
    EditText placeEditText;
    String tempText;
    AdapterSignupPlace adapterSignupPlace;
    AdapterSignupDetails adapterSignupDetails;
    final ArrayList<String> placesAll = new ArrayList<>();
    Button button3, button4;

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        adapterSignupDetails = (AdapterSignupDetails) args.getSerializable("AdapterObject");
    }

    private class AdapterSignupPlace extends ArrayAdapter<String>{

        Context applicationContext;
        int layoutResourceId;
        ArrayList<String> placesList;
        AdapterSignupPlace adapterObject;

        AdapterSignupPlace(Context context, int layoutResourceId, ArrayList<String> placesList){

            super(context, layoutResourceId, placesList);
            applicationContext = context;
            this.layoutResourceId = layoutResourceId;
            this.placesList = placesList;
            adapterObject = this;
        }

        @NonNull
        @Override
        public View getView(final int position, View convertView, @NonNull ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = ((Activity)applicationContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }
            String textToDisplay = placesList.get(position);
            TextView ticketText = (TextView) convertView.findViewById(R.id.ticket_text);
            Button closeButton = (Button) convertView.findViewById(R.id.delete_ticket);
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    placesList.remove(position);
                    adapterObject.notifyDataSetChanged();
                }
            });
            ticketText.setText(textToDisplay);
            return convertView;
        }
    }

    @Override
    public void onAttach(Context activity) {
        placeFragmentContext = (FragmentActivity) activity;
        super.onAttach(activity);
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup_place,container,false);
        placeFragmentContext.setTitle("Add your Places");
        final ListView dynamicListView = (ListView) view.findViewById(R.id.dynamic_linear_layout);
        placeEditText = (EditText)view.findViewById(R.id.place_edit_text);
        button3 = (Button) view.findViewById(R.id.button3);
        button4 = (Button)view.findViewById(R.id.button4);
        adapterSignupPlace = new AdapterSignupPlace(placeFragmentContext, R.layout.ticket_layout, placesAll);
        dynamicListView.setAdapter(adapterSignupPlace);

        button3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tempText = placeEditText.getText().toString();
                if((!tempText.equals("")) && tempText != null) {
                    placesAll.add(tempText);
                    placeEditText.setText("");
                    placeEditText.setHint("Enter a place you visited!");
                    adapterSignupPlace.notifyDataSetChanged();
                }
            }
        });
        button4.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(CheckNetworkState.checkNetworkState(placeFragmentContext))
                    executeOnClickButton4();

            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((ActivityDetails)getActivity()).setTitleView("Enter Places you visited!");
    }

    void executeOnClickButton4(){
        Intent queryServiceIntent = new Intent(placeFragmentContext, ServiceQueryIntent.class);
        queryServiceIntent.setAction("com.nitw2014.mutual.action.SET_PLACES");
        queryServiceIntent.putExtra("Places",placesAll);
        placeFragmentContext.startService(queryServiceIntent);

        adapterSignupDetails.detailsViewPager.setCurrentItem(1);

        button4.setOnClickListener(null);
    }

}
