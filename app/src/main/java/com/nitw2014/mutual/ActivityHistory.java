package com.nitw2014.mutual;

import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.bumptech.glide.request.target.Target;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class ActivityHistory extends AppCompatActivity{

    TabLayout tabLayout_main;
    ViewPager viewPager_main;
    FloatingActionButton floatingActionButton;
    DrawerLayout mDrawerLayout;
    SharedPreferences sharedPreferences;
    public static ArrayList<HashMap<String, Object>> filteringList = new ArrayList<HashMap<String, Object>>();

    public static final String AUTHORITY = "com.nitw2014.mutual.taskprovider";
    Account mAccount;
    private RelativeLayout mDrawerContentRight;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_history);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.white));

        RelativeLayout relativeLayout = (RelativeLayout) findViewById(R.id.below_statusbar_history);
        RelativeLayout.LayoutParams lp = (RelativeLayout.LayoutParams) relativeLayout.getLayoutParams();
        lp.height = getStatusBarHeight();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout_history);
        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
        mDrawerContentRight = (RelativeLayout) findViewById(R.id.below_status);


//        mDrawerLayout = getDrawerLayout();
//        mDrawerLayout.setStatusBarBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
//        mDrawerList = getDrawerList();

        floatingActionButton = (FloatingActionButton) findViewById(R.id.submit_task_main);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SubmitTaskDialogFragment submitTaskFragment = new SubmitTaskDialogFragment();
                submitTaskFragment.show(getFragmentManager(), "SubmitTaskFragment");
            }
        });

        tabLayout_main = (TabLayout) findViewById(R.id.tab_layout_main);
        tabLayout_main.setTabTextColors(getResources().getColor(R.color.white), getResources().getColor(R.color.white));
        tabLayout_main.addTab(tabLayout_main.newTab().setText("Asked"));
        tabLayout_main.addTab(tabLayout_main.newTab().setText("Shared"));
        tabLayout_main.addTab(tabLayout_main.newTab().setText("Performed"));

        viewPager_main = (ViewPager) findViewById(R.id.view_pager_main);
        MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager(), tabLayout_main.getTabCount());
        viewPager_main.setAdapter(myPagerAdapter);

        //Latest Code-------------------------------------------------------------------------------

        tabLayout_main.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab){
                viewPager_main.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewPager_main.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                tabLayout_main.getTabAt(position).select();
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//
//        sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),MODE_PRIVATE);
//        ((TextView)findViewById(R.id.user_name_history)).setText("Hi " + sharedPreferences.getString("FirstName", "NaN"));
//
//        if(!sharedPreferences.getString("ProfPicUri","NaN").equals("NaN"))
//            Glide.with(this)
//                    .load(new File(Uri.parse(sharedPreferences.getString("ProfPicUri","NaN")).getPath()))
//                    .centerCrop()
//                    .into((ImageView)findViewById(R.id.user_image_history));


    }

    //Inner Classes

    public class MyPagerAdapter extends FragmentStatePagerAdapter{
        int tabCount;

        public MyPagerAdapter(FragmentManager fm, int tabCount){
            super(fm);
            this.tabCount = tabCount;
        }

        @Override
        public Fragment getItem(int position) {
            switch(position){
                case 0: return new FragmentHistoryAsked();
                case 1: return new FragmentHistoryShared();
                case 2: return new FragmentHistoryPerformed();
                default: return null;
            }
        }

        @Override
        public int getCount() {
            return tabCount;
        }
    }

    //LATEST_CODE_START-----------------------------------------------------------------------------

    public int getStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    protected void syncDatabaseWithServer()
    {
        mAccount = Launcher.createSyncAccount(this);
        if(mAccount != null)
        {
            Log.d("ActivityDashboard","Starting Sync");
            ContentResolver.setSyncAutomatically(mAccount, AUTHORITY, true);
            Bundle settingsBundle = new Bundle();
            settingsBundle.putBoolean(
                    ContentResolver.SYNC_EXTRAS_MANUAL, true);
            settingsBundle.putBoolean(
                    ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
            ContentResolver.requestSync(mAccount, AUTHORITY, settingsBundle);
        }
    }

    protected void getDataFromContentProviderHistory(List<FeedHistoryRecycler> feedHistoryRecyclerList, Uri URI_TO_OBSERVE, String[] array, String SORT_ORDER)
    {
        Cursor cursor = getContentResolver().query(URI_TO_OBSERVE, array, null, null, SORT_ORDER);

        FeedHistoryRecycler feedHistoryRecycler;
        feedHistoryRecyclerList.clear();

        if(cursor != null)
        {
            while(cursor.moveToNext())
            {
                feedHistoryRecycler = new FeedHistoryRecycler();
                feedHistoryRecycler.setFromUserString(cursor.getString(0));
                Log.d("ActivityHistory", URI_TO_OBSERVE.toString() + " " + cursor.getString(0) + " " + cursor.getString(1));
                feedHistoryRecycler.setMainTaskString(cursor.getString(1));
                feedHistoryRecyclerList.add(feedHistoryRecycler);
            }
        }
        cursor.close();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch(item.getItemId())
        {
            case android.R.id.home:
                this.finish();
                return true;

            case R.id.notification_actionbar_history:
                return true;
            case R.id.navigation_actionbar_history:
                if(mDrawerLayout.isDrawerOpen(Gravity.END))
                    mDrawerLayout.closeDrawer(Gravity.END);
                else
                    mDrawerLayout.openDrawer(Gravity.END);
                return true;
            case R.id.home_menu:
                Intent mutualcenterIntent1 = new Intent(this, ActivityDashboard.class);
                startActivity(mutualcenterIntent1);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //LATEST_CODE_END-------------------------------------------------------------------------------

}
