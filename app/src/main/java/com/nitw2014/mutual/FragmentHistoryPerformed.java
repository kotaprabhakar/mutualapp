package com.nitw2014.mutual;

import android.app.Activity;
import android.content.Context;
import android.database.ContentObserver;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.HandlerThread;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by prabhakar on 21/08/16.
 */
public class FragmentHistoryPerformed extends Fragment {

    FragmentActivity performedFragmentContext;
    private List<FeedHistoryRecycler> feedHistoryRecyclerList;
    private RecyclerView mRecyclerView;
    private AdapterHistoryRecycler adapter;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    Handler handler, uiHandler;
    HandlerThread thread;

    PerformedObserver performedObserver;

    @Override
    public void onAttach(Context context) {
        this.performedFragmentContext = (FragmentActivity)context;
        super.onAttach(context);
    }

    @Override
    public void onDestroy() {
        performedFragmentContext.getContentResolver().unregisterContentObserver(performedObserver);
        super.onDestroy();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history_performed,container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.performed_fragment_recycler);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(performedFragmentContext));
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout_performed);

        feedHistoryRecyclerList = new ArrayList<>();
        adapter = new AdapterHistoryRecycler(performedFragmentContext, feedHistoryRecyclerList);
        mRecyclerView.setAdapter(adapter);

        thread = new HandlerThread("PerformedBackgroundThread");
        thread.start();
        handler = new Handler(thread.getLooper());
        uiHandler = new Handler(performedFragmentContext.getMainLooper());

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(true);
                ((ActivityHistory)performedFragmentContext).syncDatabaseWithServer();
                updateData();
            }
        });

        performedObserver = new PerformedObserver(handler, performedFragmentContext);
        performedFragmentContext.getContentResolver().registerContentObserver(ProviderContractTask.PerformedHistory.CONTENT_URI, true, performedObserver);

        updateData();

        return view;
    }

    void updateData()
    {
        handler.post(new Runnable() {
            @Override
            public void run() {
                ((ActivityHistory)performedFragmentContext).getDataFromContentProviderHistory(feedHistoryRecyclerList, ProviderContractTask.PerformedHistory.CONTENT_URI, new String[]{ProviderContractTask.PerformedHistory.SUBMITTER, ProviderContractTask.PerformedHistory.TASK}, ProviderContractTask.PerformedHistory.DEFAULT_SORT_ORDER);
                uiHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        adapter.notifyDataSetChanged();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }
                });
            }
        });
    }

    //--------------------------------------INNER CLASSES-------------------------------------------

     private class PerformedObserver extends ContentObserver {
        Activity activity;
        Handler handler;

        PerformedObserver(Handler handler, Activity activity){
            super(handler);
            this.activity = activity;
            this.handler = handler;
        }

        @Override
        public void onChange(boolean selfChange) {
            this.onChange(selfChange, null);
        }

        @Override
        public void onChange(boolean selfChange, Uri uri) {

            updateData();

        }
    }

}
