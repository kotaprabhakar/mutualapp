package com.nitw2014.mutual;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static android.R.id.button1;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by prabhakar on 11/06/17.
 */

public class SubmitTaskDialogFragment extends DialogFragment {

  //  EditText friendsList;
    EditText taskContent;
    ImageButton attachment;
    ImageButton emojis;
    FrameLayout attacmentLayout;
    ImageButton submitButton;
    ArrayList<String> arrayFriendsID = new ArrayList<>();
    BroadcastReceiver checkSubmitStatus = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int submitStatus = intent.getIntExtra("SubmitStatus", -1);
            if(submitStatus != -1 || submitStatus != 2){
                Toast.makeText(getActivity(), "Task Submitted Successfully", Toast.LENGTH_LONG).show();
                getDialog().cancel();
            }else{
                Toast.makeText(getActivity(), "Error Submitting Task. Please try again", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(checkSubmitStatus);
    }

    @Override
    public void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(checkSubmitStatus, new IntentFilter("CheckSubmitStatus"));

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(R.layout.activity_submit_task);

        Log.d("SubmitTaskFragment","onCreateDialog");



        return builder.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_submit_task,null,false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        /*
        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getDialog().setCanceledOnTouchOutside(true);
        */

        //-------------------------Actual Work Start------------------------------------------------
        Log.d("SubmitTaskFragment","onViewCreated");

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences(getString(R.string.mutual_shared_preferences), MODE_PRIVATE);
        String stringFriendsList = sharedPreferences.getString("FriendsList", "NaN");

        Log.d("SubmitTaskFragment", stringFriendsList);

        //submitButton = (ImageButton)view.findViewById(R.id.submit_button);
        attacmentLayout= (FrameLayout) view.findViewById(R.id.attachment_layout);

//        attachment= (ImageButton) view.findViewById(R.id.attachment);
//        attachment.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                attacmentLayout.setVisibility( View.VISIBLE);
//                Log.d("Button Click", "button clicked");
//                                if(!attacmentLayout.isShown()) {
//                      attacmentLayout.setVisibility( View.VISIBLE);
//                    // Your_Staff
//                }
//                else{
//                    attacmentLayout.setVisibility( View.GONE);
//                }
//
//            }
//        });
        //  friendsList = (EditText) view.findViewById(R.id.add_friends);

        final ListView friendsListView = (ListView) getView().findViewById(R.id.listViewFriends);

        try {
            if (stringFriendsList.equals("NaN")) {

                Toast.makeText(getActivity(), "You have no friends set up to assign tasks", Toast.LENGTH_LONG).show();

            } else {

                Log.d("ActivitySubmitTask",stringFriendsList);

                JSONArray jsonArrayFriendsList = new JSONObject(stringFriendsList).getJSONArray("var");
                final JSONArray tempjsonArrayFriendsList = jsonArrayFriendsList;
                final ArrayList<String> allFriends = new ArrayList<>();
                for(int i = 0; i < jsonArrayFriendsList.length(); i++){
                    allFriends.add(jsonArrayFriendsList.getJSONObject(i).getString("first_name") + " " + jsonArrayFriendsList.getJSONObject(i).getString("last_name"));
                }
                final ArrayAdapter<String> friendsArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.search_result_view,allFriends);

//                friendsList.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//                    @Override
//                    public void onFocusChange(View v, boolean hasFocus) {
//                        if(hasFocus){
//                            Log.d("ActivitySubmitTask","friendsList EditText has focus");
//                            friendsListView.setVisibility(View.VISIBLE);
//                            friendsListView.setAdapter(friendsArrayAdapter);
//                        }else{
//                            friendsListView.setVisibility(View.GONE);
//                        }
//                    }
//                });

                friendsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        try {
                          //  friendsList.setText(friendsList.getText() + ";" + allFriends.get(position));
                            arrayFriendsID.add(tempjsonArrayFriendsList.getJSONObject(position).getString("frndId"));
                        }catch(JSONException jsonException){
                            Log.d("ActivitySubmitTask","jsonException");
                        }
                    }
                });

                submitButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        taskContent = (EditText) view.findViewById(R.id.text_to_add);
                        String task = taskContent.getText().toString();

                        for(int j = 0; j < arrayFriendsID.size(); j++){
                            Log.d("ActivitySubmitTask",arrayFriendsID.get(j));
                        }

                        Intent serviceIntent = new Intent(getActivity(), ActivityMutualCenter.class);
                        serviceIntent.setAction("com.nitw2014.mutual.action.INSERT_TASK");
                        serviceIntent.putExtra("TaskContents", task);
                        serviceIntent.putExtra("TaskAssignees", arrayFriendsID);
                        getActivity().startService(serviceIntent);
                    }
                });
            }
        }catch(JSONException jsonException){
            Log.d("ActivitySubmitTask", "JSONException");
            if(jsonException.getMessage() != null)
                Log.d("ActivitySubmitTask", jsonException.getMessage());

        }
        //--------------------------Actual Work End-------------------------------------------------

    }
}
