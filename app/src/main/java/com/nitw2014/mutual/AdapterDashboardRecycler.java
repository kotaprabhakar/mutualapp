package com.nitw2014.mutual;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by prabhakar on 28/08/16.
 */
public class AdapterDashboardRecycler extends RecyclerView.Adapter<AdapterDashboardRecycler.DashboardViewHolder> implements ActivityDashboard.ItemTouchHelperAdapter{

    List<FeedDashboardRecycler> feedDashboardRecyclerList;
    Context applicationContext;

    public AdapterDashboardRecycler(Context context, List<FeedDashboardRecycler> feedDashboardRecyclerList){
        this.feedDashboardRecyclerList = feedDashboardRecyclerList;
        this.applicationContext = context;
    }

    @Override
    public int getItemCount() {
        return (null != feedDashboardRecyclerList ? feedDashboardRecyclerList.size():0);
    }

    @Override
    public void onBindViewHolder(DashboardViewHolder holder, int position) {
        FeedDashboardRecycler feedDashboardRecycler = feedDashboardRecyclerList.get(position);
        holder.bindModel(feedDashboardRecycler.getFromTextViewString(), feedDashboardRecycler.getTaskTextViewString());
    }

    @Override
    public DashboardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_view_dashboard, parent, false);
        return new DashboardViewHolder(view);
    }

    class DashboardViewHolder extends RecyclerView.ViewHolder{

        TextView fromTextView;
        TextView taskTextView;

        DashboardViewHolder(View itemView) {
            super(itemView);
            fromTextView = (TextView) itemView.findViewById(R.id.dashboard_title);
            taskTextView = (TextView) itemView.findViewById(R.id.dashboard_text);
        }

        void bindModel(String fromText, String taskText)
        {
            fromTextView.setText(fromText);
            taskTextView.setText(taskText);
        }

    }

    //Methods from ItemTouchHelperAdapter Interface START ------------------------------------------

    @Override
    public void onItemSwipeLeft(int position) {

    }

    @Override
    public void onItemSwipeRight(int position) {

    }

    //Methods from ItemTouchHelperAdapter Interface END   ------------------------------------------
}
