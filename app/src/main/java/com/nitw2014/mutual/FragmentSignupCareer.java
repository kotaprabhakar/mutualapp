package com.nitw2014.mutual;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Sai Karthik on 6/12/2016.
 */
public class FragmentSignupCareer extends Fragment{
    FragmentActivity careerFragmentContext;
    EditText nameEditText;
    String temp1;
    Button button7, button8;
    AdapterSignupCareer adapterSignupCareer;
    AdapterSignupDetails adapterSignupDetails;
    final ArrayList<String> professions = new ArrayList<>();
    final ArrayList<String> professions1 = new ArrayList<>();

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        adapterSignupDetails = (AdapterSignupDetails) args.getSerializable("AdapterObject");
    }

    public class AdapterSignupCareer extends ArrayAdapter<String>{
        Context applicationContext;
        int layoutResourceId;
        ArrayList<String> placesList;
        AdapterSignupCareer adapterObject;
        AdapterSignupCareer(Context context, int layoutResourceId, ArrayList<String> placesList){
            super(context, layoutResourceId, placesList);
            applicationContext = context;
            this.layoutResourceId = layoutResourceId;
            this.placesList = placesList;
            adapterObject = this;
        }

        @NonNull
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            if(convertView == null){
                LayoutInflater inflater = ((Activity) applicationContext).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }
            String textToDisplay = placesList.get(position);
            TextView ticketText = (TextView) convertView.findViewById(R.id.ticket_text);
            Button closeButton = (Button) convertView.findViewById(R.id.delete_ticket);
            closeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    placesList.remove(position);
                    adapterObject.notifyDataSetChanged();
                }
            });
            ticketText.setText(textToDisplay);
            return convertView;
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.careerFragmentContext = (FragmentActivity)context;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view  =  inflater.inflate(R.layout.fragment_signup_career,container,false);
        careerFragmentContext.setTitle("Career");
        final ListView dynamicLinearLayout = (ListView) view.findViewById(R.id.dynamic_linear_career);
        button7 = (Button) view.findViewById(R.id.button7);
        button8 = (Button)view.findViewById(R.id.button8);
        nameEditText = (EditText)view.findViewById(R.id.name_edit_text);
        adapterSignupCareer = new AdapterSignupCareer(careerFragmentContext, R.layout.ticket_layout, professions1);
        dynamicLinearLayout.setAdapter(adapterSignupCareer);

        button7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                temp1 = nameEditText.getText().toString();
                if(((!temp1.equals("")) && temp1 != null)) {
                    professions.add(temp1);
                    professions1.add(temp1);
                    professions.add("");
                    professions.add("");
                    nameEditText.setText("");
                    adapterSignupCareer.notifyDataSetChanged();
                }
            }
        });
        button8.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                if(CheckNetworkState.checkNetworkState(careerFragmentContext))
                    executeOnClickButton8();

            }
        });
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ((ActivityDetails)getActivity()).setTitleView("Enter stuff you are good at!");
    }

    void executeOnClickButton8(){

        Intent queryServiceIntent = new Intent(careerFragmentContext, ServiceQueryIntent.class);
        queryServiceIntent.setAction("com.nitw2014.mutual.action.SET_CAREER");
        queryServiceIntent.putExtra("Career",professions);
        careerFragmentContext.startService(queryServiceIntent);

        adapterSignupDetails.detailsViewPager.setCurrentItem(2);

        button8.setOnClickListener(null);

    }

}
