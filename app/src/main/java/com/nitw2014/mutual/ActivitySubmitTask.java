package com.nitw2014.mutual;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import static android.R.id.button1;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;
/**
 * Created by ssarawag on 12/16/2017.
 */

public class ActivitySubmitTask extends AppCompatActivity {

    EditText taskContent;
    ImageButton attachment;
    ImageButton emojis;
    FrameLayout attacmentLayout;
    ImageButton submitButton;
    Button next;
    TextView messageText;
    Button uploadButton;
    int serverResponseCode = 0;
    ProgressDialog dialog = null;
    ArrayList<String> arrayFriendsID = new ArrayList<>();
    BroadcastReceiver checkSubmitStatus = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int submitStatus = intent.getIntExtra("SubmitStatus", -1);
            if(submitStatus != -1 || submitStatus != 2){
                Toast.makeText(getApplicationContext(), "Task Submitted Successfully", Toast.LENGTH_LONG).show();

            }else{
                Toast.makeText(getApplicationContext(), "Error Submitting Task. Please try again", Toast.LENGTH_LONG).show();
            }
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //return super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_history, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Toolbar mutualToolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(mutualToolbar);
        SharedPreferences sharedPreferences = getSharedPreferences(getString(R.string.mutual_shared_preferences),MODE_PRIVATE);

        final SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString("TaskID","1");
        editor.apply();
        //LocalBroadcastManager.getInstance(this).registerReceiver(Successfullysent, new IntentFilter("SentEmail"));

        getWindow().setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        setContentView(R.layout.activity_submit_task);
        next = (Button)findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent uploadDoc = new Intent(getApplicationContext(), ActivityUpload.class);
                startActivity(uploadDoc);
            }
        });
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }
}